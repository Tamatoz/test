package com.stackprod.sudokux.repository

import android.content.Context
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.games.Games
import com.stackprod.sudokux.R

class AchievementsManager(val context: Context){
    fun checkCompleteLevelAchievements(id: Int, areAllLevelsResolved: Boolean){
        GoogleSignIn.getLastSignedInAccount(context)?.let { account ->
            Games.getAchievementsClient(context, account)
                    .unlock(context.getString(R.string.achievement_start))
            when (id) {
                19 -> Games.getAchievementsClient(context, account)
                        .unlock(context.getString(R.string.achievement_pupil))
                39 -> Games.getAchievementsClient(context, account)
                        .unlock(context.getString(R.string.achievement_amateur))
                59 -> Games.getAchievementsClient(context, account)
                        .unlock(context.getString(R.string.achievement_professional))
                64 -> Games.getAchievementsClient(context, account)
                        .unlock(context.getString(R.string.achievement_master))
            }
            if(areAllLevelsResolved)
                Games.getAchievementsClient(context, account).unlock(context.getString(R.string.achievement_veteran))
        }

    }
}
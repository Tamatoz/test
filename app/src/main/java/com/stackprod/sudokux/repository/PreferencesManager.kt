package com.stackprod.sudokux.repository

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences

class PreferencesManager{
    private var sPref: SharedPreferences
    constructor(context: Context){
        sPref = context.getSharedPreferences("in_app_info", MODE_PRIVATE)
    }

    fun setPurchase(skuName: String, isPurchase: Boolean){
        val editor = sPref.edit()
        editor.putBoolean(skuName, isPurchase)
        editor.apply()
    }

    fun getPurchase(skuName: String): Boolean{
        return sPref.getBoolean(skuName, false)
    }

    fun setCurrentTheme(isDark: Boolean){
        val editor = sPref.edit()
        editor.putBoolean("theme", isDark)
        editor.apply()
    }

    fun getCurrentTheme(): Boolean{
        return sPref.getBoolean("theme", false)
    }

    fun setHintsOn(on: Boolean){
        val editor = sPref.edit()
        editor.putBoolean("game_hints", on)
        editor.apply()
    }

    fun getHintsOn(): Boolean{
        return sPref.getBoolean("game_hints", true)
    }

    fun increaseRateDialogProgress(){
        val editor = sPref.edit()
        val progress = getRateDialogProgress()
        editor.putInt("rate_progress", progress)
        editor.apply()
    }

    fun setRateDialogProgress(progress: Int){
        val editor = sPref.edit()
        editor.putInt("rate_progress", progress)
        editor.apply()
    }

    fun getRateDialogProgress(): Int{
        return sPref.getInt("rate_progress", 0)
    }
}
package com.stackprod.sudokux.repository

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.games.Games
import com.google.android.gms.games.SnapshotsClient
import com.google.android.gms.games.snapshot.Snapshot
import com.google.android.gms.games.snapshot.SnapshotMetadataChange
import com.google.android.gms.tasks.Continuation
import com.google.gson.Gson
import com.stackprod.sudokux.Consts
import com.stackprod.sudokux.R
import com.stackprod.sudokux.model.Level
import com.stackprod.sudokux.model.SnapshotModel
import java.io.IOException

class SnapshotManager(val context: Context){

        fun loadSnapshot(): LiveData<Snapshot>? {
            val liveData = MutableLiveData<Snapshot>()
            if(GoogleSignIn.getLastSignedInAccount(context) == null) return null

            val snapshotsClient = Games.getSnapshotsClient(context, GoogleSignIn.getLastSignedInAccount(context)!!)

            val conflictResolutionPolicy = SnapshotsClient.RESOLUTION_POLICY_MOST_RECENTLY_MODIFIED

            snapshotsClient.open(Consts.saveGameName, true, conflictResolutionPolicy)
                    .addOnFailureListener { e -> Log.e("Error", "Error while opening Snapshot.", e) }.continueWith(Continuation<SnapshotsClient.DataOrConflict<Snapshot>, Snapshot> { task ->
                        val snapshot = task.result?.data
                        try {
                            return@Continuation snapshot
                        } catch (e: IOException) {
                            Log.e("Error", "Error while reading Snapshot.", e)
                        }
                        null
                    }).addOnCompleteListener { resultTask ->
                        resultTask.result?.let { liveData.value = it }
                    }
            return liveData
        }

    fun completeLevel(id: Int, starsCount: Int, time: Int, levelScore: Int, snapshot: Snapshot){
        var data = String(snapshot.snapshotContents.readFully())
        var snapshotModel = SnapshotModel()
        if(!data.isNullOrEmpty()) {
            snapshotModel = Gson().fromJson<SnapshotModel>(data, SnapshotModel::class.java)
        }
        if(snapshotModel.completeLevels.any { it.id == id }) {
            val existLevel = snapshotModel.completeLevels.find { it.id == id }
            existLevel?.starsCount = starsCount
            existLevel?.currentTime = time

            if((existLevel?.lBoardVal ?: 0) < levelScore) {
                existLevel?.lBoardVal = levelScore
                snapshotModel.leaderboardScore += levelScore - (existLevel?.lBoardVal ?: 0)
            }

        } else {
            val newLevel = Level()
            newLevel.id = id
            newLevel?.starsCount = starsCount
            newLevel?.currentTime = time
            newLevel?.lBoardVal = levelScore
            snapshotModel.leaderboardScore += levelScore
            snapshotModel.completeLevels.add(newLevel)
        }
        writeSnapshot(snapshot, Gson().toJson(snapshotModel).toByteArray())
    }

    fun writeSnapshot(snapshot: Snapshot?, data: ByteArray) {
        snapshot?.snapshotContents?.writeBytes(data)

        val metadataChange = SnapshotMetadataChange.Builder()
                .build()

        var snapshotsClient: SnapshotsClient? = null
        GoogleSignIn.getLastSignedInAccount(context)?.let { snapshotsClient = Games.getSnapshotsClient(context, it) }

        snapshot?.let { snapshotsClient?.commitAndClose(it, metadataChange) }
    }

    fun increaseRandomGame(snapshot: Snapshot){
        var data = String(snapshot.snapshotContents.readFully())
        var snapshotModel = SnapshotModel()
        if(!data.isNullOrEmpty()) {
            snapshotModel = Gson().fromJson<SnapshotModel>(data, SnapshotModel::class.java)
        }
        snapshotModel.completeRandomGames++
        writeSnapshot(snapshot, Gson().toJson(snapshotModel).toByteArray())
        GoogleSignIn.getLastSignedInAccount(context)?.let { account ->
            if(snapshotModel.completeRandomGames >= 10) Games.getAchievementsClient(context, account)
                    .unlock(context.getString(R.string.achievement_10_random_games))
            if(snapshotModel.completeRandomGames >= 100) Games.getAchievementsClient(context, account)
                    .unlock(context.getString(R.string.achievement_100_random_games))
        }
    }

}
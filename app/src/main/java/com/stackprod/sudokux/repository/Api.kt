package com.stackprod.sudokux.repository

import com.stackprod.sudokux.model.ResponseMarketingModel
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface Api {
    @GET("api/marketing/")
    fun marketing(@Query("id") id: String, @Query("lang") lang: String): Observable<ResponseMarketingModel>

    @GET("api/marketing/click")
    fun marketingClick(@Query("userId") userId: String, @Query("marketingId") marketingId: Long): Observable<ResponseMarketingModel>
}
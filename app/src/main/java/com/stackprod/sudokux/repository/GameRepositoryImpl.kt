package com.stackprod.sudokux.repository

import androidx.lifecycle.LiveData
import com.google.android.gms.games.snapshot.Snapshot
import com.stackprod.sudokux.model.Level
import com.stackprod.sudokux.utils.asLiveData
import io.realm.RealmResults
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GameRepositoryImpl @Inject constructor(
        private val api: Api,
        private val realmManager: RealmManager,
        private val snapshotsManager: SnapshotManager,
        private val achievementsManager: AchievementsManager
): GameRepository{

    override fun completeLevel(id: Int, starsCount: Int, time: Int, snapshot: Snapshot?){
        realmManager.completeLevel(id, starsCount, time)
        achievementsManager.checkCompleteLevelAchievements(id, realmManager.areAllLevelsResolved())
        realmManager.enableNextLevel(id)
        if(snapshot != null) {
            val levelScore = 500.times((realmManager.getLevelDifficult(id)) + 1).times(starsCount) / Math.max(time / 60, 1)
            snapshotsManager.completeLevel(id, starsCount, time, levelScore, snapshot)
        }
    }

    override fun completeLevelLocal(id: Int, starsCount: Int, time: Int){
        realmManager.completeLevelLocal(id, starsCount, time)
    }

    override fun increaseRandomGame(snapshot: Snapshot?){
        if(snapshot != null)
            snapshotsManager.increaseRandomGame(snapshot)
        realmManager.completeRandomLevel()
    }

    override fun getLevel(levelId: Int): Level {
        return realmManager.getLevel(levelId)
    }

    override fun getSnapshot(): LiveData<Snapshot>? {
        return snapshotsManager.loadSnapshot()
    }

    override fun saveLevelInstance(id: Int, instance: String, time: Int, starsCount: Int){
        realmManager.saveLevelInstance(id, instance, time, starsCount)
    }

    override fun writeSnapshot(snapshot: Snapshot?, data: ByteArray){
        snapshotsManager.writeSnapshot(snapshot, data)
    }

    override fun getLevels(difLevel: Int, isXMode: Boolean): LiveData<RealmResults<Level>>{
        return realmManager.getLevels(difLevel, isXMode).asLiveData()
    }
}
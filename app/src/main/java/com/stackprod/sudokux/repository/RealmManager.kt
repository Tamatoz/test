package com.stackprod.sudokux.repository

import com.google.android.gms.games.Games
import com.stackprod.sudokux.R
import com.stackprod.sudokux.model.Level
import io.realm.Realm
import io.realm.RealmResults

class RealmManager{

    fun getRealmInstance(): Realm{
        return Realm.getDefaultInstance()
    }

    fun getLevel(levelId: Int): Level {
        val realm = getRealmInstance()
        val level = realm.where(Level::class.java).equalTo("id", levelId).findFirst()
        realm.close()
        return level ?: Level()
    }

    fun completeLevel(id: Int, starsCount: Int, time: Int){
        val realm = getRealmInstance()
        realm.executeTransactionAsync {realm ->
            val currentLevel = realm.where(Level::class.java).equalTo("id", id).findFirst()
            currentLevel?.tempInstance = null
            currentLevel?.currentTime = time
            currentLevel?.solved = true
            currentLevel?.starsCount = starsCount
        }
        realm.close()
    }

    fun areAllLevelsResolved(): Boolean {
        return (!getRealmInstance().where(Level::class.java).findAll().any { !it.solved })
    }

    fun enableNextLevel(id: Int){
        val realm = getRealmInstance()
        realm.executeTransactionAsync {realm ->
            val nextLevel = realm.where(Level::class.java).equalTo("id", id + 1).findFirst()
            nextLevel?.enable = true
        }
        realm.close()
    }

    fun completeLevelLocal(id: Int, starsCount: Int, time: Int){
        val realm = getRealmInstance()
        realm.executeTransactionAsync {realm ->
            val currentLevel = realm.where(Level::class.java).equalTo("id", id).findFirst()
            currentLevel?.tempInstance = null
            currentLevel?.currentTime = time
            currentLevel?.solved = true
            currentLevel?.enable = true
            currentLevel?.starsCount = starsCount

            var nextLevel = realm.where(Level::class.java).equalTo("id", id + 1).findFirst()
            nextLevel?.enable = true
        }
        realm.close()
    }

    fun getLevelDifficult(id: Int): Int{
        return getRealmInstance().where(Level::class.java).equalTo("id", id).findFirst()?.difLevel ?: 0
    }

    fun saveLevelInstance(id: Int, instance: String, time: Int, starsCount: Int){
        val realm = getRealmInstance()
        realm.executeTransactionAsync {realm ->
            val level = realm.where(Level::class.java).equalTo("id", id).findFirst()
            if(level?.solved == false) {
                level?.tempInstance = instance
                level?.currentTime = time
                level?.starsCount = starsCount
            }
        }
        realm.close()
    }

    fun completeRandomLevel(){
        val realm = getRealmInstance()
        realm.executeTransactionAsync {
           realm.where(Level::class.java).equalTo("id", -1 as Int).findFirst()?.solved = true
        }
        realm.close()
    }

    fun getLevels(difLevel: Int, isXMode: Boolean): RealmResults<Level>{
        val realm = getRealmInstance()
        val result = realm.where(Level::class.java).equalTo("difLevel", difLevel).equalTo("isXMode", isXMode).findAllAsync()
        realm.close()
        return result
    }
}
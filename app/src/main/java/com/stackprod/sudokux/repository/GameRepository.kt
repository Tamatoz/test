package com.stackprod.sudokux.repository

import androidx.lifecycle.LiveData
import com.google.android.gms.games.snapshot.Snapshot
import com.stackprod.sudokux.model.Level
import io.realm.RealmResults

interface GameRepository{

    fun completeLevel(id: Int, starsCount: Int, time: Int, snapshot: Snapshot?)

    fun completeLevelLocal(id: Int, starsCount: Int, time: Int)

    fun increaseRandomGame(snapshot: Snapshot?)

    fun getLevel(levelId: Int): Level

    fun getSnapshot(): LiveData<Snapshot>?

    fun saveLevelInstance(id: Int, instance: String, time: Int, starsCount: Int)

    fun writeSnapshot(snapshot: Snapshot?, data: ByteArray)

    fun getLevels(difLevel: Int, isXMode: Boolean): LiveData<RealmResults<Level>>
}
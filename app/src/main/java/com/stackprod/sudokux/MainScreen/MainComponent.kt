package com.stackprod.sudokux.MainScreen

import com.stackprod.sudokux.di.PerActivity
import dagger.Subcomponent

@Subcomponent(modules = arrayOf(MainModule::class))
@PerActivity
interface MainComponent {
    fun inject(activity: MainActivity)
}
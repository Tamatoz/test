package com.stackprod.sudokux.MainScreen

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.games.snapshot.Snapshot
import com.stackprod.sudokux.model.ResponseMarketingModel
import com.stackprod.sudokux.repository.GameRepository
import com.stackprod.sudokux.repository.GameRepositoryImpl
import javax.inject.Inject

class MainScreenViewModel @Inject constructor(
        gameRepository: GameRepository) : ViewModel() {

    val savedSnapshot : LiveData<Snapshot>? = gameRepository.getSnapshot()
    var adShowed = false
    var playerId: String? = null
    var marketing: ResponseMarketingModel? = null
}
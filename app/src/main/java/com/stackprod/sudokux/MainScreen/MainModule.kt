package com.stackprod.sudokux.MainScreen

import com.stackprod.sudokux.App
import com.stackprod.sudokux.di.PerActivity
import com.stackprod.sudokux.repository.Api
import com.stackprod.sudokux.repository.GameRepository
import com.stackprod.sudokux.repository.GameRepositoryImpl
import com.stackprod.sudokux.repository.PreferencesManager
import dagger.Module
import dagger.Provides

@Module
class MainModule {
    @Provides
    @PerActivity
    internal fun provideMainPresenter(app: App, gameRepository: GameRepository, api: Api, preferencesManager: PreferencesManager): MainPresenter
            = MainPresenterImpl(app, gameRepository, api, preferencesManager)

}
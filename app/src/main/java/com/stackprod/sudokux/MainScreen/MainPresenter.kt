package com.stackprod.sudokux.MainScreen

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import com.android.billingclient.api.BillingClient
import com.android.billingclient.api.BillingClientStateListener
import com.google.android.gms.games.snapshot.Snapshot
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import com.stackprod.sudokux.BasePresenter
import com.stackprod.sudokux.BasePresenterImpl
import com.stackprod.sudokux.Consts
import com.stackprod.sudokux.model.ResponseMarketingModel
import com.stackprod.sudokux.model.SnapshotModel
import com.stackprod.sudokux.repository.Api
import com.stackprod.sudokux.repository.GameRepository
import com.stackprod.sudokux.repository.GameRepositoryImpl
import com.stackprod.sudokux.repository.PreferencesManager
import io.reactivex.disposables.Disposable

interface MainPresenter: BasePresenter<MainView> {
    fun onMarketingClick(playerId: String?, marketingId: Long?)
    fun loadMarkening(playerId: String?, lang: String)
    fun completeLevelLocal(id: Int, starsCount: Int, time: Int)
    fun checkSnapshotLevels(snapshot: Snapshot?)
    fun connectBillingClient()
}

class MainPresenterImpl(val context: Context, val gameRepository: GameRepository, val api: Api, val preferencesManager: PreferencesManager): BasePresenterImpl<MainView>(), MainPresenter {

    private var marketingSub: Disposable? = null

    override fun loadMarkening(playerId: String?, lang: String) {
        marketingSub = api.marketing(playerId ?: "", lang).subscribe({ response ->
            if(response?.show == true &&
                    ((response?.type == ResponseMarketingModel.MarketingType.FOR_NOT_PAYING
                            && !preferencesManager.getPurchase(Consts.inAppDisableAdBuyId))
                            || (response.type == ResponseMarketingModel.MarketingType.FOR_PAID
                            && preferencesManager.getPurchase(Consts.inAppDisableAdBuyId))
                            || response.type == ResponseMarketingModel.MarketingType.FOR_ALL
                            )){
                view?.setMarketing(response)
                val target = object: Target {
                    override fun onBitmapFailed(errorDrawable: Drawable?) {
                    }

                    override fun onPrepareLoad(placeHolderDrawable: Drawable?) {                                }

                    override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                        view?.setMarketingBitmap(bitmap)
                    }
                }
                Picasso.with(context).load(response.imageUrl).into(target)
            }
        }, {
            it.printStackTrace()
        })
    }

    override fun onMarketingClick(playerId: String?, marketingId: Long?) {
        api.marketingClick(playerId ?: "", marketingId ?: 0).subscribe()
    }

    override fun detach() {
        super.detach()
        marketingSub?.dispose()
    }

    override fun completeLevelLocal(id: Int, starsCount: Int, time: Int) {
        gameRepository.completeLevelLocal(id, starsCount, time)
    }

    override fun checkSnapshotLevels(snapshot: Snapshot?) {
        if(snapshot != null) {
            var data = String(snapshot.snapshotContents.readFully())
            if (data.isNullOrEmpty()) return
            val snapshotModel = Gson().fromJson<SnapshotModel>(String(snapshot.snapshotContents.readFully()), SnapshotModel::class.java)
            view?.setUserScore(snapshotModel.leaderboardScore)
            snapshotModel.completeLevels.forEach { level ->
                completeLevelLocal(level.id, level.starsCount, level.currentTime)
            }
        }
    }

    override fun connectBillingClient() {
        val billingClient = BillingClient.newBuilder(context).setListener(null).build()
        billingClient.startConnection(object : BillingClientStateListener {
            override fun onBillingSetupFinished(@BillingClient.BillingResponse billingResponseCode: Int) {
                if (billingResponseCode == BillingClient.BillingResponse.OK) {
                    checkPurchases(billingClient)
                }
            }
            override fun onBillingServiceDisconnected() {
            }
        })
    }

    fun checkPurchases(billingClient: BillingClient){
        val purchasesResult = billingClient.queryPurchases(BillingClient.SkuType.INAPP)
        if(purchasesResult.responseCode == 0) {
            if (purchasesResult.purchasesList.size == 0) {
                preferencesManager.setPurchase(Consts.inAppDisableAdBuyId, false)
                return
            }
            for (p in purchasesResult.purchasesList) {
                if(Gson().fromJson(p.originalJson, JsonObject::class.java).get("purchaseState").asInt == 0){
                    preferencesManager.setPurchase(p.sku, true)
                }
            }
        }
    }

}
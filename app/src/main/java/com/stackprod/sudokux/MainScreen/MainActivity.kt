package com.stackprod.sudokux.MainScreen

import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.PopupWindow
import com.google.android.gms.games.Games
import com.stackprod.sudokux.NewGameScreen.NewGameActivity
import com.stackprod.sudokux.RandomGameScreen.RandomGameActivity
import com.stackprod.sudokux.model.Level

import io.realm.Realm
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.random.Random

import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.Auth
import com.stackprod.sudokux.SettingsScreen.SettingsActivity
import com.google.android.gms.drive.Drive
import com.google.android.gms.tasks.RuntimeExecutionException
import com.stackprod.sudokux.*
import com.stackprod.sudokux.NewXGameScreen.NewXGameActivity
import com.stackprod.sudokux.model.ResponseMarketingModel
import com.stackprod.sudokux.repository.PreferencesManager
import com.stackprod.sudokux.ui.CardDialog
import com.stackprod.sudokux.utils.*
import kotlinx.android.synthetic.main.popup_promo.view.*
import java.util.*
import javax.inject.Inject


interface MainView: BaseView {
    fun setMarketing(marketing: ResponseMarketingModel)
    fun setMarketingBitmap(bitmap: Bitmap?)
    fun setUserScore(score: Long)
}

class MainActivity : BaseActivity<MainView, MainPresenter>(), MainView {

    companion object {
        val RC_SIGN_IN = 100
        val RC_ACHIEVEMENT_UI = 101
        val RC_LEADERBOARD_UI = 102
    }

    @Inject
    override lateinit var presenter: MainPresenter

    override val mvpView: MainView
        get() = this

    @Inject
    lateinit var preferencesManager: PreferencesManager

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: MainScreenViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        (application as App).appComponent.plus(MainModule()).inject(this)
        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(MainScreenViewModel::class.java)

        initMenu()
        checkGoogleAccount()
    }

    private fun checkGoogleAccount(){
        GoogleSignIn.getLastSignedInAccount(this@MainActivity)?.let { account ->
            Games.getGamesClient(this@MainActivity, account).setViewForPopups(window.decorView.findViewById(android.R.id.content))
        }

        if(!isSignedIn()){
            signInSilently()
        } else {
            loadSnapshot()
        }
    }

    private fun isSignedIn(): Boolean {
        return GoogleSignIn.getLastSignedInAccount(this) != null
    }

    private fun showAchievements() {
        GoogleSignIn.getLastSignedInAccount(this)?.let {account ->
            Games.getAchievementsClient(this, account)
                    .achievementsIntent
                    .addOnSuccessListener { intent -> startActivityForResult(intent, RC_ACHIEVEMENT_UI) }
        }
    }

    private fun showLeaderboard() {
        GoogleSignIn.getLastSignedInAccount(this)?.let { account ->
            Games.getLeaderboardsClient(this, account)
                    .getLeaderboardIntent(getString(R.string.main_leaderboard))
                    .addOnSuccessListener { intent -> startActivityForResult(intent, RC_LEADERBOARD_UI) }
        }
    }

    private fun signInSilently() {
        val signInOption = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN)
                .requestScopes(Drive.SCOPE_APPFOLDER)
                .build()
        val signInClient = GoogleSignIn.getClient(this,
                signInOption)
        signInClient.silentSignIn().addOnCompleteListener(this
        ) { task ->
            if (task.isSuccessful) {
                val signedInAccount = task.result
                loadSnapshot()
            } else {
                startSignInIntent()
            }
        }
    }
    override fun onResume() {
        super.onResume()

        val levels = Realm.getDefaultInstance().copyFromRealm<Level>(Realm.getDefaultInstance().where(Level::class.java).findAll())
        playScoreText.text = String.format("%d %%", (levels.count { it.solved && !it.isXMode }.toFloat() / levels.size * 100).toInt())

        if(preferencesManager.getPurchase(Consts.inAppDisableAdBuyId)) adView.visibility = View.GONE
        else adView.loadAd(AdRequest.Builder().build())
    }

    fun initMenu(){
        newGame.setOnClickListener {
            showMarketingDialog()?.setOnDismissListener { openNewGame() } ?: openNewGame()
        }
        randomGame.setOnClickListener {
            showMarketingDialog()?.setOnDismissListener { openRandomGame() } ?: openRandomGame()
        }
        achievements.setOnClickListener {
            showMarketingDialog()?.setOnDismissListener { openAchievements() } ?: openAchievements()
        }
        settings.setOnClickListener {
            showMarketingDialog()?.setOnDismissListener { openSettings() } ?: openSettings()
        }
        leaders.setOnClickListener {
            showMarketingDialog()?.setOnDismissListener { openLeaders() } ?: openLeaders()
        }
        modeX.setOnClickListener {
            showMarketingDialog()?.setOnDismissListener { openModeX() } ?: openModeX()
        }
    }

    private fun openNewGame(){
        val intent = Intent(this, NewGameActivity::class.java)
        intent.putExtra("xMode", false)
        startActivity(intent)
    }

    private fun openModeX(){
        val intent = Intent(this, NewXGameActivity::class.java)
        intent.putExtra("xMode", true)
        startActivity(intent)
    }

    private fun openLeaders(){
        if(isSignedIn()) showLeaderboard()
        else AlertDialog.Builder(this).setMessage(R.string.sign_in_req)
                .setNeutralButton(android.R.string.ok, null).show()
    }

    private fun openSettings(){
        startActivity(Intent(this, SettingsActivity::class.java))
    }

    private fun openAchievements(){
        if(isSignedIn()) showAchievements()
        else AlertDialog.Builder(this).setMessage(R.string.sign_in_req)
                .setNeutralButton(android.R.string.ok, null).show()
    }

    private fun openRandomGame(){
        val randomGame = Realm.getDefaultInstance().where(Level::class.java).equalTo("id", -1 as Int).findFirst()
        if (randomGame != null && !randomGame.solved) {
            showPrevGameDialog()
        } else {
            loadingDialog.show()
            AsyncTask.execute {
                generateAndStartRandom()
                runOnUiThread { loadingDialog.dismiss() }
            }
        }
    }

    private fun showPrevGameDialog(){
        val dialog = CardDialog(this)
        dialog.setContent( getString(R.string.random_warning),
                getString(android.R.string.no),
                getString(android.R.string.yes),
                View.OnClickListener {
                    loadingDialog.show()
                    AsyncTask.execute {
                        generateAndStartRandom()
                        runOnUiThread { loadingDialog.dismiss() }
                    }
                    dialog.dismiss()
                },
                View.OnClickListener {
                    val intent = Intent(this, RandomGameActivity::class.java)
                    startActivity(intent)
                    dialog.dismiss()
                })
        dialog.show()
    }

    private fun showMarketingDialog(): PopupWindow?{
        if(viewModel.marketing == null || viewModel.marketing?.bitmap == null || viewModel.adShowed) return null
        var view = View.inflate(this, R.layout.popup_promo, null)
        val pWindow = PopupWindow(view, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, true)
        pWindow.showAtLocation(window.decorView.findViewById(android.R.id.content), Gravity.CENTER,0,0)
        viewModel.adShowed = true
        view.close.setOnClickListener { pWindow.dismiss() }
        view.image.setImageBitmap(viewModel.marketing?.bitmap)
        view.setOnClickListener {
            presenter.onMarketingClick(viewModel.playerId, viewModel.marketing?.id)
            intent = Intent(Intent.ACTION_VIEW, Uri.parse(viewModel.marketing?.targetUrl))
            startActivity(intent)
        }
        viewModel.marketing?.content?.let { view.text.text = it }
        return pWindow
    }


    private fun generateAndStartRandom(){
        var lev = GameLevel(Random.nextInt(53, 56))
        val c = Level()
        c.id = -1
        c.difLevel = 0
        c.level = lev.getLevel()
        c.reference = lev.getReference()
        c.enable = true
        Realm.getDefaultInstance().executeTransaction { it.copyToRealmOrUpdate(c) }

        val intent = Intent(this, RandomGameActivity::class.java)
        startActivity(intent)
    }

    private fun startSignInIntent() {
        val signInOption = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN)
                .requestScopes(Drive.SCOPE_APPFOLDER)
                .build()
        val signInClient = GoogleSignIn.getClient(this,
                signInOption)
        val intent = signInClient.signInIntent
        startActivityForResult(intent, RC_SIGN_IN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            try {
                if (result.isSuccess) {
                    val signedInAccount = result.signInAccount
                    loadSnapshot()
                } else {
                    showSignInError()
                }
            } catch (ex: IllegalStateException){
                ex.printStackTrace()
                showSignInError()
            }
        }
    }

    fun showSignInError(){
        var message = getString(R.string.unable_sign_in)
        showToast(message)
    }


    private fun loadSnapshot(){
        viewModel.savedSnapshot?.observe(this, androidx.lifecycle.Observer {snapshot ->
            presenter.checkSnapshotLevels(snapshot)
        })

        if(isSignedIn()) {
            GoogleSignIn.getLastSignedInAccount(this)?.let { account ->
                Games.getPlayersClient(this@MainActivity, account).currentPlayerId.addOnCompleteListener {
                    try {
                        viewModel.playerId = it.result
                    } catch (ex: RuntimeExecutionException) {
                        startSignInIntent()
                        ex.printStackTrace()
                        return@addOnCompleteListener
                    }
                    presenter.loadMarkening(viewModel.playerId, Locale.getDefault().language)
                }
            }
        }

    }

    override fun setUserScore(score: Long) {
        GoogleSignIn.getLastSignedInAccount(this)?.let { account ->
            Games.getLeaderboardsClient(this, account)
                    .submitScore(getString(R.string.main_leaderboard), score)
        }
    }

    override fun setMarketing(marketing: ResponseMarketingModel) {
        viewModel.marketing = marketing
    }

    override fun setMarketingBitmap(bitmap: Bitmap?) {
        viewModel.marketing?.bitmap = bitmap
    }
}

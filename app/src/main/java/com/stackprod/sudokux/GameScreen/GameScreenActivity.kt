package com.stackprod.sudokux.GameScreen

import android.graphics.Color
import android.os.Bundle
import android.view.ViewGroup
import android.widget.RelativeLayout
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.games.Games
import com.google.gson.Gson
import com.stackprod.sudokux.model.GameCell
import com.stackprod.sudokux.ui.FontText
import com.stackprod.sudokux.ui.SudokuBoardView
import com.stackprod.sudokux.repository.PreferencesManager
import kotlinx.android.synthetic.main.activity_game.*

import java.util.*
import android.view.View
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import com.google.android.gms.games.snapshot.Snapshot
import com.stackprod.sudokux.*
import com.stackprod.sudokux.R
import com.stackprod.sudokux.ui.CardDialog
import com.stackprod.sudokux.ui.RateDialogHelper
import javax.inject.Inject

interface GameScreenView: BaseView {
    fun restartLevel()
    fun onLose()
    fun updateStarsCount(count: Int)
    fun setCellDraftValue(value: Int)
    fun setCellValue(value: Int)
    fun setNumberButtonEnable(isEnable: Boolean, buttonView: ViewGroup)
}

class GameScreenActivity : BaseActivity<GameScreenView, GameScreenPresenter>(), GameScreenView {

    override val mvpView: GameScreenView
        get() = this

    @Inject
    override lateinit var presenter: GameScreenPresenter

    lateinit var numbersButtons: Array<ViewGroup>

    @Inject lateinit var factory: GameLevelViewModel.Factory

    private val viewModel by viewModels<GameLevelViewModel> {
        factory.create(this)
    }

    @Inject
    lateinit var preferencesManager: PreferencesManager

    private lateinit var mInterstitialAd: InterstitialAd
    private var savedSnapshot: Snapshot? = null
    lateinit var timer: Timer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)
        (application as App).appComponent.plus(GameScreenModule()).inject(this)
        viewModel.enableAd = !preferencesManager.getPurchase(Consts.inAppDisableAdBuyId)
        initKeyboard()
        initUI()
    }

    private fun initUI(){
        editButton.setOnClickListener {
            if(presenter.draftMode) {
                editButtonImage.setImageResource(R.drawable.ic_edit_green)
                editButtonImage.setBackgroundColor(ContextCompat.getColor(this, R.color.almostWhite))
            }
            else {
                editButtonImage.setImageResource(R.drawable.ic_edit_gray)
                editButtonImage.setBackgroundColor(Color.WHITE)
            }
            presenter.draftMode = !presenter.draftMode
        }
        //block buttons until select cell
        (0..8).forEach { setNumberButtonEnable(false, numbersButtons[it])}
        setNumberButtonEnable(false, eraseButton)
        setNumberButtonEnable(false, editButton)
        initAd()

        GoogleSignIn.getLastSignedInAccount(this@GameScreenActivity)?.let { account ->
            Games.getGamesClient(this@GameScreenActivity, account).setViewForPopups(getParentView())
        }

        boardView.enableHints = preferencesManager.getHintsOn()
        if(preferencesManager.getCurrentTheme()) setDarkTheme()
    }

    private fun initAd(){
        mInterstitialAd = InterstitialAd(this)
        mInterstitialAd.adUnitId = getString(R.string.ad_key)
    }

    private fun setDarkTheme(){
        viewModel.darkTheme = true
        boardView.setDarkTheme()
        parentView.setBackgroundColor(ContextCompat.getColor(this, R.color.darkGrey2))
        time.setTextColor(Color.WHITE)
        numbersButtons.forEach {
            it.getChildAt(0).setBackgroundColor(ContextCompat.getColor(this, R.color.lightGrey))
            ((it.getChildAt(0) as RelativeLayout).getChildAt(0) as FontText).setTextColor(Color.WHITE)
        }
    }

    private fun initKeyboard(){
        numbersButtons = arrayOf(
            one, two, three, four, five, six, seven, eight, nine
        )
        numbersButtons.forEachIndexed { index, view ->
            view.setOnClickListener {
                presenter.setCellValue(index + 1)
            }
        }
        boardView.keyboardEnableListener = object: SudokuBoardView.KeyboardEnableListener{
            override fun onKeyboardButtonsStateChanged(isEnable: Boolean, buttons: IntRange) {
                buttons.forEach { setNumberButtonEnable(isEnable, numbersButtons[it])}
            }

            override fun onButtonEditStateChanged(isEnable: Boolean) {
                setNumberButtonEnable(isEnable, editButton)
            }

            override fun onButtonEraseStateChanged(isEnable: Boolean) {
                setNumberButtonEnable(isEnable, eraseButton)
            }

        }
        boardView.endGameListener = object: SudokuBoardView.EndGameListener{
            override fun onSuccessEndGame() {
                timer.cancel()
                presenter.completeLevel(viewModel.levelId, boardView.game.currentTime, savedSnapshot)
                showEndGameDialog(viewModel.level?.difLevel)
            }
        }

        boardView.onErrorListener = object: SudokuBoardView.ErrorListener{
            override fun onError() {
                presenter.onError()
            }
        }

        eraseButton.setOnClickListener {
            if(presenter.draftMode) boardView.eraseDraft() else boardView.erase()
        }

    }

    private fun showEndGameDialog(difLevel: Int){
        val dlg = CompleteLevelDialog(this@GameScreenActivity, presenter.errorsRemain, boardView.game.currentTime)
        dlg.setOnDismissListener {
            if(!RateDialogHelper.levelEnded(this@GameScreenActivity, difLevel))
                if(viewModel.enableAd)
                    mInterstitialAd.show()
        }
        dlg.show()
    }

    override fun restartLevel(){
        if(viewModel.enableAd) mInterstitialAd.loadAd(AdRequest.Builder().build())
        timer = Timer()
        timer.schedule(GameTimerTask(), 1000, 1000)
    }

    override fun onLose(){
        resetBoardView()
        showLoseDialog()
        if(viewModel.enableAd) mInterstitialAd.show()
    }

    private fun resetBoardView(){
//        viewModel.level.observe(this, androidx.lifecycle.Observer {currentLevel ->
            boardView.game.currentTime = 0
            timer.cancel()
            boardView.setGameMatrix(viewModel.level.convertToMatrix()
                    ?: Array(9) { kotlin.Array(9) { com.stackprod.sudokux.model.GameCell() } })
            boardView.setGameRefMatrix(viewModel.level.convertRefToMatrix()
                    ?: Array(9) { kotlin.Array(9) { com.stackprod.sudokux.model.GameCell() } })
            boardView.inErrorMode = false
            presenter.resetErrors()
//        })
    }

    override fun updateStarsCount(count: Int){
        when(count){
            0 -> starsCount.setImageResource(R.drawable.star0)
            1 -> starsCount.setImageResource(R.drawable.star1)
            2 -> starsCount.setImageResource(R.drawable.star2)
            3 -> starsCount.setImageResource(R.drawable.star3)
        }
    }

    override fun setCellValue(value: Int){
        boardView.setCellValue(value)
    }

    override fun setCellDraftValue(value: Int){
        boardView.setCellDraftValue(value)
    }

    override fun setNumberButtonEnable(isEnable: Boolean, buttonView: ViewGroup){
        if(isEnable) buttonView.getChildAt(0).setBackgroundColor(if(viewModel.darkTheme) ContextCompat.getColor(this, R.color.lightGrey) else Color.WHITE)
        else buttonView.getChildAt(0).setBackgroundColor(if(viewModel.darkTheme) ContextCompat.getColor(this, R.color.darkGrey) else ContextCompat.getColor(this, R.color.activeWhite))

        buttonView.isEnabled = isEnable
    }

    override fun onPause() {
        super.onPause()
        presenter.saveCurrentLevelInstance(viewModel.levelId, Gson().toJson(boardView.game.matrix), boardView.game.currentTime)
        timer.cancel()

    }

    override fun onResume() {
        super.onResume()
        viewModel.savedSnapshot?.observe(this, androidx.lifecycle.Observer { snapshot ->
            savedSnapshot = snapshot
        })
//        viewModel.level.observe(this, androidx.lifecycle.Observer { currentLevel->
            if(viewModel.level.starsCount != 0) presenter.errorsRemain = viewModel.level.starsCount ?: 3
            boardView.setGameMatrix(Gson().fromJson<Array<Array<GameCell>>>(viewModel.level.tempInstance, Array<Array<GameCell>>::class.java) ?: viewModel.level.convertToMatrix() ?: Array(9) { kotlin.Array(9) { com.stackprod.sudokux.model.GameCell() } })
            boardView.setGameRefMatrix(viewModel.level.convertRefToMatrix() ?: Array(9) { kotlin.Array(9) { com.stackprod.sudokux.model.GameCell() } })
            boardView.game.currentTime = viewModel.level.currentTime ?: 0
            boardView.isXMode = viewModel.level.isXMode ?: false
            timer = Timer()
            timer.schedule(GameTimerTask(), 1000, 1000)
//        })

        if(viewModel.enableAd) adView.loadAd(AdRequest.Builder().build())
        else adView.visibility = View.GONE
    }

    fun showLoseDialog(){
        val dialog = CardDialog(this)
        dialog.setContent( getString(R.string.you_lose),
                getString(R.string.to_menu),
                getString(R.string.replay),
                View.OnClickListener {
                    dialog.dismiss()
                    onBackPressed()
                },
                View.OnClickListener {
                    dialog.dismiss()
                    restartLevel()
                })
        dialog.show()
    }

    inner class GameTimerTask : TimerTask() {
        override fun run() {
            boardView.game.currentTime++
            runOnUiThread { time.text = String.format("%d:%02d", boardView.game.currentTime / 60, boardView.game.currentTime % 60) }
        }
    }

}
package com.stackprod.sudokux.GameScreen

import com.stackprod.sudokux.di.PerActivity
import dagger.Subcomponent

@Subcomponent(modules = arrayOf(GameScreenModule::class))
@PerActivity
interface GameScreenComponent {
    fun inject(activity: GameScreenActivity)
}
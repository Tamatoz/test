package com.stackprod.sudokux.GameScreen

import com.stackprod.sudokux.di.PerActivity
import com.stackprod.sudokux.repository.GameRepository
import com.stackprod.sudokux.repository.GameRepositoryImpl
import dagger.Module
import dagger.Provides


@Module
class GameScreenModule {
    @Provides
    @PerActivity
    internal fun provideGameScreenPresenter(gameRepository: GameRepository): GameScreenPresenter = GameScreenPresenterImpl(gameRepository)



}
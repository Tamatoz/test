package com.stackprod.sudokux.GameScreen

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.stackprod.sudokux.BaseActivity
import com.stackprod.sudokux.LevelsScreen.LevelsActivity
import com.stackprod.sudokux.R
import kotlinx.android.synthetic.main.dg_complete_level.*

class CompleteLevelDialog(val ctx: Activity, private val starsCountVal: Int, private val timeVal: Int): Dialog(ctx) {

    init {
        setCancelable(false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dg_complete_level)
        time.text = String.format("%d:%02d", timeVal / 60, timeVal % 60)
        when(starsCountVal){
            0 -> starsCount.setImageResource(R.drawable.star0)
            1 -> starsCount.setImageResource(R.drawable.star1)
            2 -> starsCount.setImageResource(R.drawable.star2)
            3 -> starsCount.setImageResource(R.drawable.star3)
        }
        levelsList.setOnClickListener {
            dismiss()
            ctx.finish()
//            var intent = Intent(context, LevelsActivity::class.java)
//            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
//            intent.putExtra("xMode", xMode)
//            context.startActivity(intent)
        }
        window?.setBackgroundDrawableResource(android.R.color.transparent)
    }
}
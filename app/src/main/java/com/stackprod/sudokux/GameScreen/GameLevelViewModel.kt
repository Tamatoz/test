package com.stackprod.sudokux.GameScreen

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.AbstractSavedStateVMFactory
import androidx.lifecycle.LiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.google.android.gms.games.snapshot.Snapshot
import com.stackprod.sudokux.model.Level
import com.stackprod.sudokux.repository.GameRepository
import com.stackprod.sudokux.repository.GameRepositoryImpl
import javax.inject.Inject

class GameLevelViewModel constructor(
        savedStateHandle: SavedStateHandle,
        gameRepository: GameRepository) : ViewModel() {

    val levelId : Int = savedStateHandle["levelId"] ?:
    throw IllegalArgumentException("levelId not passed")

    val level : Level = gameRepository.getLevel(levelId)
    val savedSnapshot : LiveData<Snapshot>? = gameRepository.getSnapshot()
    var darkTheme = false
    var enableAd = false

    class Factory @Inject constructor(private val gameRepository: GameRepository) {
        fun create(owner: FragmentActivity): AbstractSavedStateVMFactory {
            return object : AbstractSavedStateVMFactory(owner, owner.intent.extras) {
                override fun <T : ViewModel> create(
                        key: String,
                        modelClass: Class<T>,
                        handle: SavedStateHandle
                ): T {
                    return GameLevelViewModel(handle, gameRepository) as T
                }
            }
        }
    }
}
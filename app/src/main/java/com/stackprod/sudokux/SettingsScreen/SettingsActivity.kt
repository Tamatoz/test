package com.stackprod.sudokux.SettingsScreen

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.billingclient.api.*
import com.google.android.gms.games.snapshot.Snapshot
import com.stackprod.sudokux.App
import com.stackprod.sudokux.BaseActivity
import com.stackprod.sudokux.BaseView
import com.stackprod.sudokux.R
import com.stackprod.sudokux.repository.PreferencesManager
import kotlinx.android.synthetic.main.activity_settings.*
import javax.inject.Inject

interface SettingsView: BaseView {
    fun showResetStatWarning()
    fun buyDisableAd()
}

class SettingsActivity : BaseActivity<SettingsView, SettingsPresenter>(), SettingsView, PurchasesUpdatedListener {

    override val mvpView: SettingsView
        get() = this

    @Inject
    override lateinit var presenter: SettingsPresenter

    lateinit var adapter: SettingsAdapter
    lateinit private var billingClient: BillingClient

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var preferencesManager: PreferencesManager

    private lateinit var viewModel: SettingsViewModel

    var savedSnapshot: Snapshot? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        (application as App).appComponent.plus(SettingsModule()).inject(this)
        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(SettingsViewModel::class.java)

        adapter = SettingsAdapter(this, preferencesManager, presenter)
        list.adapter = adapter
        list.layoutManager = LinearLayoutManager(this)
        backButton.setOnClickListener { onBackPressed() }

        billingClient = BillingClient.newBuilder(this).setListener(this).build()
        billingClient.startConnection(object : BillingClientStateListener {
            override fun onBillingSetupFinished(@BillingClient.BillingResponse billingResponseCode: Int) {
                if (billingResponseCode == BillingClient.BillingResponse.OK) {
                }
            }
            override fun onBillingServiceDisconnected() {
            }
        })
        viewModel.savedSnapshot?.observe(this, Observer {  snapshot ->
            savedSnapshot = snapshot
        })
    }

    override fun buyDisableAd(){
        val flowParams = BillingFlowParams.newBuilder()
                .setSku("disable_ad")
                .setType(BillingClient.SkuType.INAPP)
                .build()
        val responseCode = billingClient.launchBillingFlow(this, flowParams)
    }

    override fun onPurchasesUpdated(responseCode: Int, purchases: MutableList<Purchase>?) {
        if (responseCode == BillingClient.BillingResponse.OK && purchases != null) {
            for (purchase in purchases) {
                presenter.setPurchased(purchase.sku)
            }
        }
    }

    override fun showResetStatWarning(){
        AlertDialog.Builder(this)
                .setMessage(R.string.reset_warning)
                .setPositiveButton(android.R.string.yes) { p0, p1 ->
                    presenter.wipeData(savedSnapshot)
                }
                .setNegativeButton(android.R.string.no) { p0, p1 ->
                    p0.dismiss()
                }
                .show()
    }

}
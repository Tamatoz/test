package com.stackprod.sudokux.SettingsScreen

import android.content.Context
import com.google.android.gms.games.snapshot.Snapshot
import com.google.gson.Gson
import com.stackprod.sudokux.BasePresenter
import com.stackprod.sudokux.BasePresenterImpl
import com.stackprod.sudokux.R
import com.stackprod.sudokux.model.Level
import com.stackprod.sudokux.model.SnapshotModel
import com.stackprod.sudokux.repository.GameRepository
import com.stackprod.sudokux.repository.PreferencesManager
import com.stackprod.sudokux.repository.GameRepositoryImpl
import io.realm.Realm

interface SettingsPresenter: BasePresenter<SettingsView> {
    fun buyDisableAd()
    fun changeTheme()
    fun resetStat()
    fun setPurchased(name: String)
    fun wipeData(snapshot: Snapshot?)
    fun changeHintsOn()
}

class SettingsPresenterImpl(val context: Context, val preferencesManager: PreferencesManager, val gameRepository: GameRepository): BasePresenterImpl<SettingsView>(), SettingsPresenter {

    override fun buyDisableAd(){
        view?.buyDisableAd()
    }

    override fun changeTheme(){
        preferencesManager.setCurrentTheme(!preferencesManager.getCurrentTheme())
    }

    override fun resetStat(){
        view?.showResetStatWarning()
    }

    override fun setPurchased(name: String){
        preferencesManager.setPurchase(name, true)
    }

    override fun wipeData(snapshot: Snapshot?){
        val realm = Realm.getDefaultInstance()
        val levelsList = mutableListOf<Level>()
        val levelsResource = context.resources.getStringArray(R.array.levels)
        levelsResource.forEach {
            levelsList.add(Gson().fromJson<Level>(it, Level::class.java))
        }
        realm.executeTransaction {
            realm.deleteAll()
            realm.copyToRealm(levelsList)
        }
        gameRepository.writeSnapshot(
                snapshot,
                Gson().toJson(SnapshotModel()).toByteArray())
    }

    override fun changeHintsOn(){
        preferencesManager.setHintsOn(!preferencesManager.getHintsOn())
    }
}
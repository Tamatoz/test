package com.stackprod.sudokux.SettingsScreen

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.games.snapshot.Snapshot
import com.stackprod.sudokux.repository.GameRepository
import com.stackprod.sudokux.repository.GameRepositoryImpl
import javax.inject.Inject

class SettingsViewModel @Inject constructor(
        gameRepository: GameRepository) : ViewModel() {

    val savedSnapshot : LiveData<Snapshot>? = gameRepository.getSnapshot()

}
package com.stackprod.sudokux.SettingsScreen

import com.stackprod.sudokux.di.PerActivity
import dagger.Subcomponent

@Subcomponent(modules = arrayOf(SettingsModule::class))
@PerActivity
interface SettingsComponent {
    fun inject(activity: SettingsActivity)
}
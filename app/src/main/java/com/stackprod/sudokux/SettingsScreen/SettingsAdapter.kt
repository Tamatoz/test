package com.stackprod.sudokux.SettingsScreen

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.stackprod.sudokux.Consts
import com.stackprod.sudokux.R
import com.stackprod.sudokux.repository.PreferencesManager
import kotlinx.android.synthetic.main.row_disable_ad.view.*


class SettingsAdapter(val context: Context, val preferences: PreferencesManager, val presenter: SettingsPresenter): RecyclerView.Adapter<SettingsAdapter.SettingsViewHolder>(){

    private val items: MutableList<ListItem> = mutableListOf()
    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)

    init {
        fillItems()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SettingsViewHolder {
        return SettingsViewHolder(layoutInflater.inflate(ItemType.values()[viewType].layoutRes, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: SettingsViewHolder, position: Int) {
        when (ItemType.values()[items[position].type.ordinal]) {
            ItemType.DisableAd -> {
                holder.itemView.settingTitle.setText(R.string.disable_ad)
                holder.itemView.settingContent.setText(R.string.disable_ad_content)
                holder.itemView.icon.setImageResource(R.drawable.ic_disable_ad)
                holder.itemView.setOnClickListener {
                    presenter.buyDisableAd()
                }
            }
            ItemType.ChangeTheme -> {
                val isDarkTheme = preferences.getCurrentTheme()
                holder.itemView.setBackgroundColor(if(isDarkTheme) ContextCompat.getColor(context, R.color.grey) else Color.WHITE)
                holder.itemView.settingTitle.setTextColor(if(isDarkTheme) Color.WHITE else Color.BLACK)
                holder.itemView.settingContent.setTextColor(if(isDarkTheme) ContextCompat.getColor(context, R.color.activeWhite) else ContextCompat.getColor(context, R.color.lightGrey2))
                holder.itemView.settingTitle.setText(R.string.change_theme)
                holder.itemView.settingContent.setText(if(isDarkTheme) R.string.dark else R.string.light)
                holder.itemView.icon.setImageResource(if(isDarkTheme) R.drawable.baseline_remove_red_eye_white_48 else R.drawable.baseline_remove_red_eye_black_48)
                holder.itemView.setOnClickListener {
                    presenter.changeTheme()
                    notifyDataSetChanged()
                }
            }
            ItemType.RestoreSettings -> {
                holder.itemView.settingTitle.setText(R.string.reset_stat)
                holder.itemView.settingContent.setText(R.string.reset_stat_content)
                holder.itemView.icon.setImageResource(R.drawable.round_settings_backup_restore_black_48)
                holder.itemView.setOnClickListener {
                    presenter.resetStat()
                }
            }
            ItemType.GameHints -> {
                holder.itemView.settingTitle.setText(R.string.game_hint)
                holder.itemView.settingContent.setText(if(preferences.getHintsOn()) R.string.on else R.string.off)
                holder.itemView.icon.setImageResource(R.drawable.sharp_border_inner_black_48)
                holder.itemView.setOnClickListener {
                    presenter.changeHintsOn()
                    notifyDataSetChanged()
                }
            }
        }
    }

    private fun fillItems() {
        items.clear()
        if(!preferences.getPurchase(Consts.inAppDisableAdBuyId))
            items.add(ListItem(ItemType.DisableAd))
        items.add(ListItem(ItemType.ChangeTheme))
        items.add(ListItem(ItemType.GameHints))
        items.add(ListItem(ItemType.RestoreSettings))
    }


    inner class SettingsViewHolder(view: View): RecyclerView.ViewHolder(view)

    private data class ListItem(val type: ItemType, val serviceIndex: Int? = null, val text1: String? = null, val text2: String? = null)

    private enum class ItemType(@LayoutRes val layoutRes: Int) {
        DisableAd(R.layout.row_disable_ad),
        ChangeTheme(R.layout.row_disable_ad),
        GameHints(R.layout.row_disable_ad),
        RestoreSettings(R.layout.row_disable_ad)
    }

}
package com.stackprod.sudokux.SettingsScreen

import com.stackprod.sudokux.App
import com.stackprod.sudokux.di.PerActivity
import com.stackprod.sudokux.repository.GameRepository
import com.stackprod.sudokux.repository.GameRepositoryImpl
import com.stackprod.sudokux.repository.PreferencesManager
import dagger.Module
import dagger.Provides

@Module
class SettingsModule {
    @Provides
    @PerActivity
    internal fun provideRandomGamePresenter(context: App, preferences: PreferencesManager, gameRepository: GameRepository): SettingsPresenter
            = SettingsPresenterImpl(context, preferences, gameRepository)

}
package com.stackprod.sudokux.ModeXScreen

import com.stackprod.sudokux.di.PerActivity
import com.stackprod.sudokux.repository.GameRepository
import com.stackprod.sudokux.repository.GameRepositoryImpl
import dagger.Module
import dagger.Provides

@Module
class ModeXScreenModule {
    @Provides
    @PerActivity
    internal fun provideModeXGamePresenter(gameRepository: GameRepository): ModeXGamePresenter = ModeXGamePresenterImpl(gameRepository)

}
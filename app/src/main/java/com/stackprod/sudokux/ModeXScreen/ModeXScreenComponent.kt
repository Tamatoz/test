package com.stackprod.sudokux.ModeXScreen

import com.stackprod.sudokux.di.PerActivity
import dagger.Subcomponent


@Subcomponent(modules = arrayOf(ModeXScreenModule::class))
@PerActivity
interface ModeXScreenComponent {
    fun inject(activity: ModeXGameActivity)
}
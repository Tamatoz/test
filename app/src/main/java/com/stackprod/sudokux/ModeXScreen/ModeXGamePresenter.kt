package com.stackprod.sudokux.ModeXScreen

import com.google.android.gms.games.snapshot.Snapshot
import com.stackprod.sudokux.BasePresenter
import com.stackprod.sudokux.BasePresenterImpl
import com.stackprod.sudokux.repository.GameRepository
import com.stackprod.sudokux.repository.GameRepositoryImpl

interface ModeXGamePresenter: BasePresenter<ModeXGameView> {
    var draftMode: Boolean
    var errorsRemain: Int
    fun setCellValue(value: Int)
    fun onError()
    fun resetErrors()
    fun completeLevel(id: Int, time: Int, snapshot: Snapshot?)
    fun saveCurrentLevelInstance(id: Int, instance: String, time: Int)
}


class ModeXGamePresenterImpl(val gameRepository: GameRepository): BasePresenterImpl<ModeXGameView>(), ModeXGamePresenter {

    override var draftMode = false
    override var errorsRemain = 3

    override fun setCellValue(value: Int){
        if(draftMode) view?.setCellDraftValue(value)
        else view?.setCellValue(value)
    }

    override fun onError(){
        errorsRemain--
        if(errorsRemain == 0){
            view?.onLose()
        }
        view?.updateStarsCount(errorsRemain)
    }

    override fun resetErrors(){
        errorsRemain = 3
        view?.updateStarsCount(errorsRemain)
    }

    override fun completeLevel(id: Int, time: Int, snapshot: Snapshot?) {
        gameRepository.completeLevel(id, errorsRemain, time, snapshot)
    }

    override fun saveCurrentLevelInstance(id: Int, instance: String, time: Int) {
        gameRepository.saveLevelInstance(id, instance, time, errorsRemain)
    }
}
package com.stackprod.sudokux

import androidx.multidex.MultiDexApplication
import com.android.billingclient.api.BillingClient
import com.android.billingclient.api.BillingClientStateListener
import com.android.billingclient.api.Purchase
import com.android.billingclient.api.PurchasesUpdatedListener
import com.google.android.gms.ads.MobileAds
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.stackprod.sudokux.model.Level
import com.stackprod.sudokux.repository.PreferencesManager
import io.realm.Realm
import io.realm.RealmConfiguration



class App : MultiDexApplication(), PurchasesUpdatedListener {

    private var billingClient: BillingClient? = null

    lateinit var appComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .build()
        Realm.init(this)
        MobileAds.initialize(this, "ca-app-pub-1456087496532483~8264242877")
        val levelsList = mutableListOf<Level>()
        val levelsResource = resources.getStringArray(R.array.levels)
        levelsResource.forEach {
            levelsList.add(Gson().fromJson<Level>(it, Level::class.java))
        }

        val config = RealmConfiguration.Builder()
                .name("realm.db")
                .initialData { realm ->
                    levelsList.forEach {
                        val c = Level()
                        c.id = it.id
                        c.difLevel = it.difLevel
                        c.level = it.level
                        c.reference = it.reference
                        c.enable = it.enable
                        c.isXMode = it.isXMode
                        realm.copyToRealmOrUpdate(c)
                    }
                }
                .deleteRealmIfMigrationNeeded()
                .build()
        Realm.setDefaultConfiguration(config)

        billingClient = BillingClient.newBuilder(this).setListener(this).build()

        billingClient?.startConnection(object : BillingClientStateListener {
            override fun onBillingSetupFinished(@BillingClient.BillingResponse billingResponseCode: Int) {
                if (billingResponseCode == BillingClient.BillingResponse.OK) {
                    checkPurchases()
                }
            }
            override fun onBillingServiceDisconnected() {
            }
        })
    }


    private fun checkPurchases(){
        val preferencesManager = PreferencesManager(this)
        val purchasesResult = billingClient?.queryPurchases(BillingClient.SkuType.INAPP)
        if(purchasesResult?.responseCode == 0) {
            if (purchasesResult.purchasesList.size == 0) {
                preferencesManager.setPurchase(Consts.inAppDisableAdBuyId, false)
                return
            }
            for (p in purchasesResult.purchasesList) {
                if(Gson().fromJson(p.originalJson, JsonObject::class.java).get("purchaseState").asInt == 0){
                    preferencesManager.setPurchase(p.sku, true)
                }
            }
        }
    }

    override fun onPurchasesUpdated(responseCode: Int, purchases: MutableList<Purchase>?) {

    }
}
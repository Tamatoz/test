package com.stackprod.sudokux.model

class SnapshotModel{
    val completeLevels = mutableListOf<Level>()
    var completeRandomGames = 0
    var leaderboardScore = 0L
}
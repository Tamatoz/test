package com.stackprod.sudokux.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class Level: RealmObject() {
    @PrimaryKey
    var id = -1
    var difLevel = 0
    var level: String? = null
    var reference: String? = null
    var solved: Boolean = false
    var isXMode = false
    var starsCount = 0
    var enable = false
    var tempInstance: String? = null
    var currentTime = 0
    var lBoardVal = 0

    fun convertToMatrix(): Array<Array<GameCell>>{
        val matrix = Array(9) {Array(9) {GameCell()} }

        level?.toCharArray()?.map { Integer.parseInt(it.toString()) }?.forEachIndexed {index, value ->
            val gc = GameCell()
            gc.currentValue = value
            matrix[index / 9][index % 9] = gc
        }
        return matrix
    }

    fun convertRefToMatrix(): Array<Array<GameCell>>{
        val matrix = Array(9) {Array(9) {GameCell()} }

        reference?.toCharArray()?.map { Integer.parseInt(it.toString()) }?.forEachIndexed {index, value ->
            val gc = GameCell()
            gc.currentValue = value
            matrix[index / 9][index % 9] = gc
        }
        return matrix
    }
}
package com.stackprod.sudokux.model

import android.graphics.Bitmap

class ResponseMarketingModel {

    var id: Long = 0
    var show: Boolean? = null

    var description: String? = null

    var content: String? = null

    var imageUrl: String? = null

    var targetUrl: String? = null

    var type: MarketingType? = null
    var bitmap: Bitmap? = null

    constructor()

    enum class MarketingType {
        FOR_ALL,
        FOR_PAID,
        FOR_NOT_PAYING

    }

}
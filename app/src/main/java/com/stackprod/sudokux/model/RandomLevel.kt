package com.stackprod.sudokux.model

import java.io.Serializable

class RandomLevel: Serializable{
    var level: String? = null
    var reference: String? = null

    fun convertToMatrix(): Array<Array<GameCell>>{
        val matrix = Array(9) {Array(9) {GameCell()} }

        level?.toCharArray()?.map { Integer.parseInt(it.toString()) }?.forEachIndexed {index, value ->
            val gc = GameCell()
            gc.currentValue = value
            matrix[index / 9][index % 9] = gc
        }
        return matrix
    }

    fun convertRefToMatrix(): Array<Array<GameCell>>{
        val matrix = Array(9) {Array(9) {GameCell()} }

        reference?.toCharArray()?.map { Integer.parseInt(it.toString()) }?.forEachIndexed {index, value ->
            val gc = GameCell()
            gc.currentValue = value
            matrix[index / 9][index % 9] = gc
        }
        return matrix
    }
}
package com.stackprod.sudokux.model

class Game{
    var sectors = Array(9) {Array(3) {Array(3) {GameCell()}} }
    var matrix = Array(9) {Array(9) {GameCell()} }
    set(value) {
        field = value
        val setEditable = !field.any { it.any { !it.editable } }
        matrix.forEachIndexed { row, arrayOfGameCells ->
            arrayOfGameCells.forEachIndexed { column, gameCell ->
                if(setEditable && gameCell.currentValue != 0) gameCell.editable = false
                sectors[(row / 3) * 3 + column / 3][row % 3][column % 3] = gameCell

            }
        }
    }

    var refMatrix = Array(9) {Array(9) {GameCell()} }

    var currentTime = 0

}
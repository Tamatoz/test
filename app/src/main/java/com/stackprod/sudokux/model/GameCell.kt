package com.stackprod.sudokux.model

class GameCell{
    var currentValue = 0
    var draftValues = mutableSetOf<Int>()
    var editable = true
    var error = false
}
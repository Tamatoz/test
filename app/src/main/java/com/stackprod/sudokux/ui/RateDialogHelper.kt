package com.stackprod.sudokux.ui

import android.content.Context
import com.stackprod.sudokux.repository.PreferencesManager

class RateDialogHelper{

    companion object {
        fun levelEnded(context: Context, dif: Int): Boolean{
            val preferencesManager = PreferencesManager(context)
            var rateProgress = preferencesManager.getRateDialogProgress()
            if(rateProgress == -1) return false
            if(rateProgress < 5 && (dif == 0 || dif == 1)){
                preferencesManager.increaseRateDialogProgress()
                return showRateDialog(context, preferencesManager)
            } else if(rateProgress >= 5){
                preferencesManager.increaseRateDialogProgress()
                return showRateDialog(context, preferencesManager)
            } else return false
        }

        fun showRateDialog(context: Context, preferencesManager: PreferencesManager): Boolean{
            var rateProgress = preferencesManager.getRateDialogProgress()
            if(rateProgress == 5 || (rateProgress > 5 || (rateProgress - 5) % 20 == 0)) {
                val dialog = RateDialog(context)
                dialog.show()
                return true
            }
            return false
        }
    }
}
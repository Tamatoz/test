package com.stackprod.sudokux.ui

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import com.stackprod.sudokux.model.GameCell
import android.view.MotionEvent
import android.graphics.Paint.Align
import android.graphics.Typeface
import androidx.core.content.ContextCompat
import com.stackprod.sudokux.R
import com.stackprod.sudokux.model.Game
import com.stackprod.sudokux.utils.dpToPx


class SudokuBoardView: View {

    private val numbersCount = IntArray(9)
    var isXMode = false

    var enableHints = true
    fun setDarkTheme(){
        gridColor = ContextCompat.getColor(context, R.color.white2)
        gridErrorColor = ContextCompat.getColor(context, R.color.lightPink)
        chooseColor = ContextCompat.getColor(context, R.color.darkGreen)
        sameNumberColor = ContextCompat.getColor(context, R.color.orange)
        chooseSectorColor = ContextCompat.getColor(context, R.color.darkGrey)
        chooseErrorColor = ContextCompat.getColor(context, R.color.red)
        initView()
    }

    var currentTouchCoords: Pair<Float, Float>? = null
    var currentSelectedCell: GameCell? = null
    var currentSelectedCellCoords = Pair(-1, -1)

    var inErrorMode = false

    var game = Game()
    fun setGameMatrix(matrix: Array<Array<GameCell>>) {
        game.matrix = matrix
        if(matrix.any { it.any { it.error } }) inErrorMode = true
        currentSelectedCell = null
        currentSelectedCellCoords = Pair(-1, -1)
        currentTouchCoords = null
        invalidate()
        calcNumbersCount()
    }

    fun setGameRefMatrix(matrix: Array<Array<GameCell>>) {
        game.refMatrix = matrix
    }

    var keyboardEnableListener: KeyboardEnableListener? = null
    var endGameListener: EndGameListener? = null
    var onErrorListener: ErrorListener? = null

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initView()
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        initView()
    }

    private var gridColor = Color.BLACK
    private var gridErrorColor = ContextCompat.getColor(context, R.color.pink)
    private var chooseColor = ContextCompat.getColor(context, R.color.lightBlue)
    private var sameNumberColor = ContextCompat.getColor(context, R.color.yellow)
    private var chooseSectorColor = ContextCompat.getColor(context, R.color.white3)
    private var chooseErrorColor = ContextCompat.getColor(context, R.color.lightPink2)
    private lateinit var gridPaint: Paint
    private lateinit var boldGridPaint: Paint
    private lateinit var boldGridErrorPaint: Paint
    private lateinit var selectedGridPaint: Paint
    private lateinit var textPaint: Paint
    private lateinit var draftTextPaint: Paint
    private lateinit var choosePaint: Paint
    private lateinit var sameNumberPaint: Paint
    private lateinit var chooseSectorPaint: Paint
    private lateinit var chooseErrorPaint: Paint


    fun initView(){
        choosePaint = Paint()
        choosePaint.color = chooseColor

        chooseSectorPaint = Paint()
        chooseSectorPaint.color = chooseSectorColor

        sameNumberPaint = Paint()
        sameNumberPaint.color = sameNumberColor

        chooseErrorPaint = Paint()
        chooseErrorPaint.color = chooseErrorColor

        gridPaint = Paint()
        gridPaint.color = gridColor
        gridPaint.strokeWidth = 2f
        gridPaint.style = Paint.Style.STROKE

        boldGridPaint = Paint()
        boldGridPaint.color = gridColor
        boldGridPaint.strokeWidth = 10f
        boldGridPaint.style = Paint.Style.STROKE

        boldGridErrorPaint = Paint()
        boldGridErrorPaint.color = gridErrorColor
        boldGridErrorPaint.strokeWidth = 10f
        boldGridErrorPaint.style = Paint.Style.STROKE

        selectedGridPaint = Paint()
        selectedGridPaint.color = gridColor
        selectedGridPaint.strokeWidth = 15f

        val plain = Typeface.createFromAsset(context.assets, "Comfortaa-Bold.ttf")
        val scale = context.resources.displayMetrics.density

        textPaint = Paint()
//        textPaint.textSize = TEXT_SIZE * scale + 0.5f
        textPaint.color = gridColor
        textPaint.textAlign = Align.CENTER
        textPaint.typeface = plain

        draftTextPaint = Paint()
//        draftTextPaint.textSize = DRAFT_TEXT_SIZE * scale + 0.5f
        draftTextPaint.color = gridColor
        draftTextPaint.textAlign = Align.CENTER
        draftTextPaint.typeface = plain
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        var cell = height / 9F
        var draftCell = (cell - (boldGridPaint.strokeWidth * 2)) / 3F
        var sector = height / 3F

        setTextSizeForWidth(draftTextPaint, draftCell - context.dpToPx(1f), "0")
        setTextSizeForWidth(textPaint, cell - context.dpToPx(16f), "0")

        game.matrix.forEachIndexed { row, arrayOfGameCells ->
            arrayOfGameCells.forEachIndexed { column, gameCell ->
                val cellLeft = cell * column
                val cellTop = cell * row

                val sectorLeft = sector * (column / 3)
                val sectorTop = sector * (row / 3)


                currentTouchCoords?.let {
                    if(enableHints) {
                        if (it.first in sectorLeft..sectorLeft + sector && it.second in sectorTop..sectorTop + sector) {
                            canvas?.drawRect(cellLeft, cellTop, cellLeft + cell, cellTop + cell, chooseSectorPaint)
                        }
                        if (it.first in cellLeft..cellLeft + cell || it.second in cellTop..cellTop + cell) {
                            canvas?.drawRect(cellLeft, cellTop, cellLeft + cell, cellTop + cell, choosePaint)
                        }
                        if(isXMode && (currentSelectedCellCoords.first == currentSelectedCellCoords.second || currentSelectedCellCoords.first == 8 - currentSelectedCellCoords.second) && (row == column || row == 8 - column)){
                            canvas?.drawRect(cellLeft, cellTop, cellLeft + cell, cellTop + cell, choosePaint)
                        }
                        if (gameCell.currentValue != 0 && gameCell.currentValue == currentSelectedCell?.currentValue) {
                            canvas?.drawRect(cellLeft, cellTop, cellLeft + cell, cellTop + cell, sameNumberPaint)
                        }
                    }
                    if(it.first in cellLeft..cellLeft + cell && it.second in cellTop..cellTop + cell){
                        canvas?.drawRect(cellLeft + boldGridPaint.strokeWidth / 2, cellTop + boldGridPaint.strokeWidth / 2, cellLeft + cell - boldGridPaint.strokeWidth / 2, cellTop + cell - boldGridPaint.strokeWidth / 2, boldGridPaint)
                    }
                }
                canvas?.drawRect(cellLeft, cellTop, cellLeft + cell, cellTop + cell, gridPaint)


                if(gameCell.error){
                    canvas?.drawRect(cellLeft, cellTop, cellLeft + cell, cellTop + cell, chooseErrorPaint)
                    canvas?.drawRect(cellLeft + boldGridErrorPaint.strokeWidth / 2, cellTop + boldGridErrorPaint.strokeWidth / 2, cellLeft + cell - boldGridErrorPaint.strokeWidth / 2, cellTop + cell - boldGridErrorPaint.strokeWidth / 2, boldGridErrorPaint)
                }



                if(gameCell.currentValue != 0)
                    canvas?.let {
                        drawTextCentred(it, gameCell.currentValue.toString(), cellLeft + cell / 2, cellTop + cell / 2, textPaint)
                    }
                else if(gameCell.draftValues.isNotEmpty()){
                    canvas?.let { canvas ->
                        gameCell.draftValues.forEach {
                            drawTextCentred(canvas, it.toString(), cellLeft + boldGridPaint.strokeWidth + ((it - 1) % 3 * draftCell) + draftCell / 2, cellTop + boldGridPaint.strokeWidth + ((it - 1) / 3 * draftCell) + draftCell / 2, draftTextPaint)
                        }
                    }

                }



            }
        }

        canvas?.drawLine(cell * 3, 0f, cell * 3, height.toFloat(), boldGridPaint)
        canvas?.drawLine(cell * 6, 0f, cell * 6, height.toFloat(), boldGridPaint)

        canvas?.drawLine(0f, cell * 3, width.toFloat(), cell * 3, boldGridPaint)
        canvas?.drawLine(0f, cell * 6, width.toFloat(), cell * 6, boldGridPaint)
    }

    private val textBounds = Rect()
    fun drawTextCentred(canvas: Canvas, text: String, cx: Float, cy: Float, paint: Paint) {
        paint.getTextBounds(text, 0, text.length, textBounds)
        canvas.drawText(text, cx , cy - textBounds.exactCenterY(), paint)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val widthMode = View.MeasureSpec.getMode(widthMeasureSpec)
        val widthSize = View.MeasureSpec.getSize(widthMeasureSpec)
        val heightMode = View.MeasureSpec.getMode(heightMeasureSpec)
        val heightSize = View.MeasureSpec.getSize(heightMeasureSpec)

        val desiredWidth = Math.min(widthSize, heightSize)
        val desiredHeight = Math.min(widthSize, heightSize)

        val width: Int
        val height: Int

        //Measure Width
        if (widthMode == View.MeasureSpec.EXACTLY) {
            //Must be this size
            width = widthSize
        } else if (widthMode == View.MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            width = Math.min(desiredWidth, widthSize)
        } else {
            //Be whatever you want
            width = desiredWidth
        }

        //Measure Height
        if (heightMode == View.MeasureSpec.EXACTLY) {
            //Must be this size
            height = heightSize
        } else if (heightMode == View.MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            height = Math.min(desiredHeight, heightSize)
        } else {
            //Be whatever you want
            height = desiredHeight
        }

        setMeasuredDimension(width, height)
    }

    fun setCellValue(value: Int){
        if(currentSelectedCell == null) return
        currentSelectedCell?.currentValue = value
        if(game.refMatrix[currentSelectedCellCoords.first][currentSelectedCellCoords.second].currentValue != value){
            currentSelectedCell?.error = true
            inErrorMode = true
            onErrorListener?.onError()

            keyboardEnableListener?.onKeyboardButtonsStateChanged(false, 0..8)
            keyboardEnableListener?.onButtonEditStateChanged(false)
            keyboardEnableListener?.onButtonEraseStateChanged(true)
        } else {
            keyboardEnableListener?.onKeyboardButtonsStateChanged(false, 0..8)
            keyboardEnableListener?.onButtonEditStateChanged(false)
            keyboardEnableListener?.onButtonEraseStateChanged(false)
            currentSelectedCell?.editable = false
            currentSelectedCell?.error = false
            inErrorMode = false
            if(!game.matrix.any { it.any{ it.currentValue == 0 } }) endGameListener?.onSuccessEndGame()
        }
        invalidate()
        calcNumbersCount()
    }

    private fun calcNumbersCount(){
        numbersCount.fill(0)
        game.matrix.forEach { it.forEach { if(it.currentValue != 0) numbersCount[it.currentValue - 1]++ } }
        numbersCount.forEachIndexed { index, i ->  if(i == 9) keyboardEnableListener?.onKeyboardButtonsStateChanged(false, IntRange(index, index)) }
    }

    fun setCellDraftValue(value: Int){
        if(inErrorMode) return
        if(currentSelectedCell?.draftValues?.contains(value) == true) currentSelectedCell?.draftValues?.remove(value)
        else currentSelectedCell?.draftValues?.add(value)
        invalidate()
    }


    override fun onTouchEvent(event: MotionEvent): Boolean {
        val cell = width / 9

        currentSelectedCellCoords = Pair(Math.min(event.y.toInt(), height) / cell, Math.min(event.x.toInt(), width) / cell)
        try {
            currentSelectedCell = game.matrix[currentSelectedCellCoords.first][currentSelectedCellCoords.second]
        } catch (ex: ArrayIndexOutOfBoundsException){
            ex.printStackTrace()
            return true
        }
        currentTouchCoords = Pair(event.x, event.y)

        if(currentSelectedCell?.editable == true) {
            keyboardEnableListener?.onKeyboardButtonsStateChanged(true, 0..8)
            keyboardEnableListener?.onButtonEditStateChanged(true)
            keyboardEnableListener?.onButtonEraseStateChanged(true)
        }
        else {
            keyboardEnableListener?.onKeyboardButtonsStateChanged(false, 0..8)
            keyboardEnableListener?.onButtonEditStateChanged(false)
            keyboardEnableListener?.onButtonEraseStateChanged(false)
        }

        if(inErrorMode) {
            if(currentSelectedCell?.error != true) {
                keyboardEnableListener?.onKeyboardButtonsStateChanged(false, 0..8)
                keyboardEnableListener?.onButtonEditStateChanged(false)
                keyboardEnableListener?.onButtonEraseStateChanged(false)
            } else {
                keyboardEnableListener?.onKeyboardButtonsStateChanged(false, 0..8)
                keyboardEnableListener?.onButtonEditStateChanged(false)
                keyboardEnableListener?.onButtonEraseStateChanged(true)
            }
        }

        numbersCount.forEachIndexed { index, i ->  if(i == 9) keyboardEnableListener?.onKeyboardButtonsStateChanged(false, IntRange(index, index)) }

        invalidate()

        return true
    }

    fun erase(){
        if(currentSelectedCell?.error == true){
            keyboardEnableListener?.onKeyboardButtonsStateChanged(true, 0..8)
            keyboardEnableListener?.onButtonEditStateChanged(true)
            keyboardEnableListener?.onButtonEraseStateChanged(true)
        }
        currentSelectedCell?.error = false
        currentSelectedCell?.currentValue = 0
        inErrorMode = false
        invalidate()
    }

    fun eraseDraft(){
        currentSelectedCell?.draftValues?.clear()
        invalidate()
    }


    interface KeyboardEnableListener{
        fun onKeyboardButtonsStateChanged(isEnable: Boolean, buttons: IntRange)
        fun onButtonEditStateChanged(isEnable: Boolean)
        fun onButtonEraseStateChanged(isEnable: Boolean)
    }

    interface EndGameListener{
        fun onSuccessEndGame()
    }

    interface ErrorListener{
        fun onError()
    }

    private fun setTextSizeForWidth(paint: Paint, desiredHeight: Float,
                                    text: String) {
        val testTextSize = 48f
        paint.textSize = testTextSize
        val bounds = Rect()
        paint.getTextBounds(text, 0, text.length, bounds)
        val desiredTextSize = testTextSize * desiredHeight/ bounds.height()
        paint.textSize = desiredTextSize
    }

}
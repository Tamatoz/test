package com.stackprod.sudokux.ui

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import com.stackprod.sudokux.R
import com.stackprod.sudokux.repository.PreferencesManager
import kotlinx.android.synthetic.main.dialog_alert.*

import android.content.Intent
import android.net.Uri


class RateDialog(context: Context): Dialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_alert)
        text.text = context.getText(R.string.rate_message)
        action1.text = context.getText(R.string.no_rate)
        action2.text = context.getText(R.string.later)
        action3.text = context.getText(R.string.rate)
        action3.visibility = View.VISIBLE
        action1.setOnClickListener {
            PreferencesManager(context).setRateDialogProgress(-1)
            dismiss()
        }
        action2.setOnClickListener { dismiss() }
        action3.setOnClickListener {
            context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.stackprod.sudokux")))
            dismiss()
        }

        window?.setBackgroundDrawableResource(android.R.color.transparent)
    }
}
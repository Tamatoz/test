package com.stackprod.sudokux.ui

import android.content.Context
import android.util.AttributeSet
import android.widget.TextView
import android.graphics.Typeface
import android.util.Log
import com.stackprod.sudokux.R


class FontText: TextView {

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        setCustomFont(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        setCustomFont(context, attrs)
    }

    private fun setCustomFont(ctx: Context, attrs: AttributeSet) {
        val a = ctx.obtainStyledAttributes(attrs, R.styleable.FontText)
        val customFont = a.getString(R.styleable.FontText_typefaceAsset)
        setCustomFont(ctx, customFont)
        a.recycle()
    }

    fun setCustomFont(ctx: Context, asset: String?): Boolean {
        var tf: Typeface? = null
        try {
            tf = Typeface.createFromAsset(ctx.assets, asset)
        } catch (e: Exception) {
            Log.e(TAG, "Could not get typeface: " + e.message)
            return false
        }

        typeface = tf
        return true
    }

    companion object {
        private val TAG = "TextView"
    }

}
package com.stackprod.sudokux.ui

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import com.stackprod.sudokux.R
import kotlinx.android.synthetic.main.dialog_alert.*

class CardDialog(context: Context): Dialog(context) {

    lateinit var textVal:String
    lateinit var act1Name: String
    lateinit var act2Name: String
    lateinit var act1Listener: View.OnClickListener
    lateinit var act2Listener: View.OnClickListener
    fun setContent(textVal:String, act1Name: String, act2Name: String, act1Listener: View.OnClickListener, act2Listener: View.OnClickListener){
        this.textVal = textVal
        this.act1Name = act1Name
        this.act2Name = act2Name
        this.act1Listener = act1Listener
        this.act2Listener = act2Listener
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_alert)
        text.text = textVal
        action1.text = act1Name
        action2.text = act2Name
        action1.setOnClickListener(act1Listener)
        action2.setOnClickListener(act2Listener)

        window?.setBackgroundDrawableResource(android.R.color.transparent)
    }
}
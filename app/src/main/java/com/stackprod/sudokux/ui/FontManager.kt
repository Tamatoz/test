package com.stackprod.sudokux.ui

import android.text.TextUtils
import android.graphics.Typeface
import android.content.res.AssetManager


class FontManager (private val mgr: AssetManager) {

    private val fonts: MutableMap<String, Typeface>

    init {
        fonts = HashMap()
    }

    fun getFont(asset: String): Typeface? {
        if (fonts.containsKey(asset))
            return fonts[asset]

        var font: Typeface? = null

        try {
            font = Typeface.createFromAsset(mgr, asset)
            fonts[asset] = font
        } catch (e: Exception) {

        }

        if (font == null) {
            try {
                val fixedAsset = fixAssetFilename(asset)
                font = Typeface.createFromAsset(mgr, fixedAsset)
                fonts[asset] = font
                fonts[fixedAsset] = font
            } catch (e: Exception) {

            }

        }

        return font
    }

    private fun fixAssetFilename(asset: String): String {
        var asset = asset
        // Empty font filename?
        // Just return it. We can't help.
        if (TextUtils.isEmpty(asset))
            return asset

        // Make sure that the font ends in '.ttf' or '.ttc'
        if (!asset.endsWith(".ttf") && !asset.endsWith(".ttc"))
            asset = String.format("%s.ttf", asset)

        return asset
    }

}
package com.stackprod.sudokux

interface BasePresenter<V: BaseView> {
    var isAttached: Boolean
    fun attach(view: V)
    fun detach()
}
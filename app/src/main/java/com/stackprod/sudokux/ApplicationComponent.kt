package com.stackprod.sudokux
import com.stackprod.sudokux.GameScreen.GameScreenComponent
import com.stackprod.sudokux.GameScreen.GameScreenModule
import com.stackprod.sudokux.LevelsScreen.LevelsScreenComponent
import com.stackprod.sudokux.LevelsScreen.LevelsScreenModule
import com.stackprod.sudokux.MainScreen.MainComponent
import com.stackprod.sudokux.MainScreen.MainModule
import com.stackprod.sudokux.ModeXScreen.ModeXScreenComponent
import com.stackprod.sudokux.ModeXScreen.ModeXScreenModule
import com.stackprod.sudokux.NewGameScreen.NewGameComponent
import com.stackprod.sudokux.NewGameScreen.NewGameModule
import com.stackprod.sudokux.NewXGameScreen.NewXGameComponent
import com.stackprod.sudokux.NewXGameScreen.NewXGameModule
import com.stackprod.sudokux.RandomGameScreen.RandomGameComponent
import com.stackprod.sudokux.RandomGameScreen.RandomGameModule
import com.stackprod.sudokux.SettingsScreen.SettingsComponent
import com.stackprod.sudokux.SettingsScreen.SettingsModule
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = arrayOf(ApplicationModule::class))
interface ApplicationComponent {

    fun plus(module: GameScreenModule): GameScreenComponent
    fun plus(module: LevelsScreenModule): LevelsScreenComponent
    fun plus(module: ModeXScreenModule): ModeXScreenComponent
    fun plus(module: NewGameModule): NewGameComponent
    fun plus(module: NewXGameModule): NewXGameComponent
    fun plus(module: RandomGameModule): RandomGameComponent
    fun plus(module: SettingsModule): SettingsComponent
    fun plus(module: MainModule): MainComponent
}
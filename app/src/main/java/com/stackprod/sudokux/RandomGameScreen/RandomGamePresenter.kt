package com.stackprod.sudokux.RandomGameScreen

import com.google.android.gms.games.snapshot.Snapshot
import com.stackprod.sudokux.BasePresenter
import com.stackprod.sudokux.BasePresenterImpl
import com.stackprod.sudokux.repository.GameRepository
import com.stackprod.sudokux.repository.GameRepositoryImpl

interface RandomGamePresenter: BasePresenter<RandomGameView> {
    var draftMode: Boolean
    var errorsRemain: Int
    fun setCellValue(value: Int)
    fun onError()
    fun resetErrors()
    fun increaseRandomGame(snapshot: Snapshot?)
    fun saveCurrentLevelInstance(instance: String, time: Int)
}

class RandomGamePresenterImpl(val gameRepository: GameRepository): BasePresenterImpl<RandomGameView>(), RandomGamePresenter {

    override var draftMode = false
    override var errorsRemain = 3

    override fun setCellValue(value: Int){
        if(draftMode) view?.setCellDraftValue(value)
        else view?.setCellValue(value)
    }

    override fun onError(){
        errorsRemain--
        if(errorsRemain == 0){
            view?.onLose()
        }
        view?.updateStarsCount(errorsRemain)
    }

    override fun resetErrors(){
        errorsRemain = 3
        view?.updateStarsCount(errorsRemain)
    }

    override fun increaseRandomGame(snapshot: Snapshot?) {
        gameRepository.increaseRandomGame(snapshot)
    }

    override fun saveCurrentLevelInstance(instance: String, time: Int) {
        gameRepository.saveLevelInstance(-1, instance, time, errorsRemain)
    }
}
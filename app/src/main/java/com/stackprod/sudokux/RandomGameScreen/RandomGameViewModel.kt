package com.stackprod.sudokux.RandomGameScreen

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.games.snapshot.Snapshot
import com.stackprod.sudokux.model.Level
import com.stackprod.sudokux.repository.GameRepository
import com.stackprod.sudokux.repository.GameRepositoryImpl
import javax.inject.Inject

class RandomGameViewModel @Inject constructor(gameRepository: GameRepository) : ViewModel() {

    val savedSnapshot : LiveData<Snapshot>? = gameRepository.getSnapshot()
    val level : Level = gameRepository.getLevel(-1)
    var darkTheme = false
    var enableAd = false
}
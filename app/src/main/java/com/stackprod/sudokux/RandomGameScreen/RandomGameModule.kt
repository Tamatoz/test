package com.stackprod.sudokux.RandomGameScreen

import com.stackprod.sudokux.di.PerActivity
import com.stackprod.sudokux.repository.GameRepository
import com.stackprod.sudokux.repository.GameRepositoryImpl
import dagger.Module
import dagger.Provides

@Module
class RandomGameModule {
    @Provides
    @PerActivity
    internal fun provideRandomGamePresenter(gameRepository: GameRepository): RandomGamePresenter = RandomGamePresenterImpl(gameRepository)

}
package com.stackprod.sudokux.RandomGameScreen

import android.app.AlertDialog
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.google.android.gms.games.snapshot.Snapshot
import com.google.gson.Gson
import com.stackprod.sudokux.*
import com.stackprod.sudokux.model.GameCell
import com.stackprod.sudokux.ui.FontText
import com.stackprod.sudokux.ui.SudokuBoardView
import com.stackprod.sudokux.repository.PreferencesManager
import kotlinx.android.synthetic.main.activity_game.*
import java.util.*
import javax.inject.Inject

interface RandomGameView: BaseView {
    fun restartLevel()
    fun onLose()
    fun updateStarsCount(count: Int)
    fun setCellDraftValue(value: Int)
    fun setCellValue(value: Int)
    fun setNumberButtonEnable(isEnable: Boolean, buttonView: ViewGroup)
}

class RandomGameActivity : BaseActivity<RandomGameView, RandomGamePresenter>(), RandomGameView {

    override val mvpView: RandomGameView
        get() = this

    @Inject
    override lateinit var presenter: RandomGamePresenter

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: RandomGameViewModel

    private lateinit var numbersButtons: Array<ViewGroup>


    private lateinit var mInterstitialAd: InterstitialAd

    @Inject
    lateinit var preferencesManager: PreferencesManager

    private var savedSnapshot: Snapshot? = null
    lateinit var timer: Timer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)
        (application as App).appComponent.plus(RandomGameModule()).inject(this)
        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(RandomGameViewModel::class.java)

        viewModel.enableAd = !preferencesManager.getPurchase(Consts.inAppDisableAdBuyId)
        initKeyboard()
        initUI()
    }

    private fun initUI(){
        editButton.setOnClickListener {
            if(presenter.draftMode) {
                editButtonImage.setImageResource(R.drawable.ic_edit_green)
                editButtonImage.setBackgroundColor(ContextCompat.getColor(this, R.color.almostWhite))
            }
            else {
                editButtonImage.setImageResource(R.drawable.ic_edit_gray)
                editButtonImage.setBackgroundColor(Color.WHITE)
            }
            presenter.draftMode = !presenter.draftMode
        }
        //block buttons until select cell
        (0..8).forEach { setNumberButtonEnable(false, numbersButtons[it])}
        setNumberButtonEnable(false, eraseButton)
        setNumberButtonEnable(false, editButton)

        initAd()

        boardView.enableHints = preferencesManager.getHintsOn()
        if(preferencesManager.getCurrentTheme()) setDarkTheme()
    }

    private fun initAd(){
        mInterstitialAd = InterstitialAd(this)
        mInterstitialAd.adUnitId = getString(R.string.ad_key)
    }


    private fun setDarkTheme(){
        viewModel.darkTheme = true
        boardView.setDarkTheme()
        parentView.setBackgroundColor(ContextCompat.getColor(this, R.color.darkGrey2))
        time.setTextColor(Color.WHITE)
        numbersButtons.forEach {
            it.getChildAt(0).setBackgroundColor(ContextCompat.getColor(this, R.color.lightGrey))
            ((it.getChildAt(0) as RelativeLayout).getChildAt(0) as FontText).setTextColor(Color.WHITE)
        }
    }


    private fun initKeyboard(){
        numbersButtons = arrayOf(
                one, two, three, four, five, six, seven, eight, nine
        )
        numbersButtons.forEachIndexed { index, view ->
            view.setOnClickListener {
                presenter.setCellValue(index + 1)
            }
        }
        boardView.keyboardEnableListener = object: SudokuBoardView.KeyboardEnableListener{
            override fun onKeyboardButtonsStateChanged(isEnable: Boolean, buttons: IntRange) {
                buttons.forEach { setNumberButtonEnable(isEnable, numbersButtons[it])}
            }

            override fun onButtonEditStateChanged(isEnable: Boolean) {
                setNumberButtonEnable(isEnable, editButton)
            }

            override fun onButtonEraseStateChanged(isEnable: Boolean) {
                setNumberButtonEnable(isEnable, eraseButton)
            }

        }
        boardView.endGameListener = object: SudokuBoardView.EndGameListener{
            override fun onSuccessEndGame() {
                timer.cancel()
                presenter.increaseRandomGame(savedSnapshot)
                showEndGameDialog()
            }
        }

        boardView.onErrorListener = object: SudokuBoardView.ErrorListener{
            override fun onError() {
                presenter.onError()
            }
        }

        eraseButton.setOnClickListener {
            if(presenter.draftMode) boardView.eraseDraft() else boardView.erase()
        }


        viewModel.savedSnapshot?.observe(this, androidx.lifecycle.Observer {
            savedSnapshot = it
        })
    }

    private fun showEndGameDialog(){
        val dlg = AlertDialog.Builder(this@RandomGameActivity)
                .setMessage(R.string.congrats)
                .setPositiveButton(R.string.to_menu) { dialog, int ->
                    dialog.dismiss()
                    onBackPressed()
                }
                .setCancelable(false)
                .create()

        dlg.setOnDismissListener {
            if(viewModel.enableAd) mInterstitialAd.show()
        }
        dlg.show()

    }

    override fun restartLevel(){
        boardView.setGameMatrix(viewModel.level.convertToMatrix())
        boardView.setGameRefMatrix(viewModel.level.convertRefToMatrix())
        boardView.inErrorMode = false
        presenter.resetErrors()
        if(viewModel.enableAd) mInterstitialAd.loadAd(AdRequest.Builder().build())
    }

    override fun updateStarsCount(count: Int){
        when(count){
            0 -> starsCount.setImageResource(R.drawable.star0)
            1 -> starsCount.setImageResource(R.drawable.star1)
            2 -> starsCount.setImageResource(R.drawable.star2)
            3 -> starsCount.setImageResource(R.drawable.star3)
        }
    }

    override fun setCellValue(value: Int){
        boardView.setCellValue(value)
    }

    override fun setCellDraftValue(value: Int){
        boardView.setCellDraftValue(value)
    }

    override fun setNumberButtonEnable(isEnable: Boolean, buttonView: ViewGroup){
        if(isEnable) buttonView.getChildAt(0).setBackgroundColor(if(viewModel.darkTheme) ContextCompat.getColor(this, R.color.lightGrey) else Color.WHITE)
        else buttonView.getChildAt(0).setBackgroundColor(if(viewModel.darkTheme) ContextCompat.getColor(this, R.color.darkGrey) else ContextCompat.getColor(this, R.color.activeWhite))

        buttonView.isEnabled = isEnable
    }

    override fun onPause() {
        super.onPause()
        presenter.saveCurrentLevelInstance(Gson().toJson(boardView.game.matrix), boardView.game.currentTime)
        timer.cancel()
    }

    override fun onLose(){
        if(viewModel.enableAd) mInterstitialAd.show()
        showLoseDialog()
    }

    private fun showLoseDialog(){
        AlertDialog.Builder(this@RandomGameActivity)
                .setMessage(R.string.you_lose)
                .setPositiveButton(R.string.replay) { dialog, int ->
                    dialog.dismiss()
                    restartLevel()
                }
                .setNegativeButton(R.string.to_menu) { dialog, int ->
                    dialog.dismiss()
                    onBackPressed()
                }
                .setCancelable(false)
                .show()
    }

    override fun onResume() {
        super.onResume()
        boardView.setGameMatrix(Gson().fromJson<Array<Array<GameCell>>>(viewModel.level.tempInstance, Array<Array<GameCell>>::class.java) ?: viewModel.level.convertToMatrix() ?: Array(9) { kotlin.Array(9) { com.stackprod.sudokux.model.GameCell() } })
        boardView.setGameRefMatrix(viewModel.level.convertRefToMatrix())
        boardView.game.currentTime = viewModel.level.currentTime

        timer = Timer()
        timer.schedule(GameTimerTask(), 1000, 1000)

        if(viewModel.enableAd) adView.loadAd(AdRequest.Builder().build())
        else adView.visibility = View.GONE
    }

    inner class GameTimerTask : TimerTask() {

        override fun run() {
            boardView.game.currentTime++
            runOnUiThread { time.text = String.format("%d:%02d", boardView.game.currentTime / 60, boardView.game.currentTime % 60) }
        }
    }

}
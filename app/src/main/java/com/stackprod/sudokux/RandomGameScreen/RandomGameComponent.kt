package com.stackprod.sudokux.RandomGameScreen

import com.stackprod.sudokux.di.PerActivity
import dagger.Subcomponent

@Subcomponent(modules = arrayOf(RandomGameModule::class))
@PerActivity
interface RandomGameComponent {
    fun inject(activity: RandomGameActivity)
}
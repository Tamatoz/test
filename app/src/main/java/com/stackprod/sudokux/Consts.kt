package com.stackprod.sudokux

class Consts{
    companion object {
        val saveGameName = "save_levels"
        val inAppDisableAdBuyId = "disable_ad"
        val apiHost = "https://marketing.stackprod.com/"
    }

}
package com.stackprod.sudokux

import android.app.Activity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner
import com.stackprod.sudokux.ui.LoadingDialog

abstract class BaseActivity<V : BaseView, P : BasePresenter<V>> : FragmentActivity(){
    abstract val presenter: P
    abstract val mvpView: V

    lateinit var loadingDialog: LoadingDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadingDialog = LoadingDialog(this)
    }

    fun showToast(text: String){
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
    }

    fun showLoading(){
        loadingDialog.show()
    }

    fun hideLoading(){
        loadingDialog.dismiss()
    }

    fun getParentView(): View = window.decorView.findViewById(android.R.id.content)

    override fun onResume() {
        super.onResume()
        presenter.attach(mvpView)
    }

    override fun onPause() {
        presenter.detach()
        super.onPause()
    }

}
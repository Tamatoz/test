package com.stackprod.sudokux.NewXGameScreen

import com.stackprod.sudokux.di.PerActivity
import dagger.Subcomponent

@Subcomponent(modules = arrayOf(NewXGameModule::class))
@PerActivity
interface NewXGameComponent {
    fun inject(activity: NewXGameActivity)
}
package com.stackprod.sudokux.NewXGameScreen

import com.stackprod.sudokux.BasePresenter
import com.stackprod.sudokux.BasePresenterImpl

interface NewXGamePresenter: BasePresenter<NewXGameView> {

}

class NewXGamePresenterImpl: BasePresenterImpl<NewXGameView>(), NewXGamePresenter {

}
package com.stackprod.sudokux.NewXGameScreen

import com.stackprod.sudokux.di.PerActivity
import dagger.Module
import dagger.Provides

@Module
class NewXGameModule {
    @Provides
    @PerActivity
    internal fun provideNewXGamePresenter(): NewXGamePresenter = NewXGamePresenterImpl()

}
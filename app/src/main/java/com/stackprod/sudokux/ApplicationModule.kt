package com.stackprod.sudokux

import com.stackprod.sudokux.di.ViewModelModule
import com.stackprod.sudokux.repository.*
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class ApplicationModule(private val app: App){

    @Provides
    @Singleton
    internal fun provideApplication(): App {
        return app
    }


    @Provides
    @Singleton
    internal fun provideGameRepository(api: Api, realmManager: RealmManager, snapshotManager: SnapshotManager, achievementsManager: AchievementsManager): GameRepository {
        return GameRepositoryImpl(api, realmManager, snapshotManager, achievementsManager)
    }

    @Provides
    @Singleton
    internal fun provideApi(): Api {
        val retrofit = Retrofit.Builder()
                .baseUrl(Consts.apiHost)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        return retrofit.create(Api::class.java)
    }

    @Provides
    @Singleton
    internal fun provideRealmManager(): RealmManager {
        return RealmManager()
    }

    @Provides
    @Singleton
    internal fun provideSnapshotsManager(app: App): SnapshotManager {
        return SnapshotManager(app)
    }

    @Provides
    @Singleton
    internal fun provideAchievementsManager(app: App): AchievementsManager {
        return AchievementsManager(app)
    }

    @Provides
    @Singleton
    internal fun providePreferencesManager(app: App): PreferencesManager {
        return PreferencesManager(app)
    }

}
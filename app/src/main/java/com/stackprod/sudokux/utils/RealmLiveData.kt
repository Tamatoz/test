package com.stackprod.sudokux.utils

import androidx.lifecycle.LiveData
import io.realm.RealmChangeListener
import io.realm.RealmModel
import io.realm.RealmObject
import io.realm.RealmResults


class RealmLiveData <T : RealmModel>(val realmResults: RealmResults<T>) : LiveData<RealmResults<T>>() {

    private val listener = RealmChangeListener<RealmResults<T>> { results -> value = results }

    override fun onActive() {
        realmResults.addChangeListener(listener)
    }

    override fun onInactive() {
        realmResults.removeChangeListener(listener)
    }
}

class RealmObjectLiveData <T : RealmObject>(val realmResult: T) : LiveData<T>() {

    private val listener = RealmChangeListener<T> { result -> value = result }

    override fun onActive() {
        realmResult.addChangeListener(listener)
    }

    override fun onInactive() {
        realmResult.removeChangeListener(listener)
    }
}
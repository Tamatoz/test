package com.stackprod.sudokux.utils

import io.realm.RealmModel
import io.realm.RealmObject
import io.realm.RealmResults

fun <T: RealmModel> RealmResults<T>.asLiveData() = RealmLiveData<T>(this)

fun <T: RealmObject> RealmObject.asLiveData() = RealmObjectLiveData<T>(this as T)
package com.stackprod.sudokux.utils

import java.util.*

class GameLevel{


    var reference: Array<IntArray> = Array(9, { IntArray(9,{0}) })

    var complexity: Int = 81

    constructor(complexity: Int){
        this.complexity = complexity
        this.reference =  sudoGen()
    }

    fun getLevel():String{
        val rnd = (1..81).shuffled().subList(0,this.complexity).sorted()
        val data = this.reference
        var result = StringBuilder()
        var count = 1
        for(row in data){
            for (cell in row){
                if (rnd.contains(count)) {
                    result.append("0")
                }
                else result.append("$cell")

                count++
            }
        }
        return result.toString()
    }


    fun getReference():String{
        val data = this.reference
        var result = StringBuilder()
        val count = 1
        for(row in data){
            for (cell in row){
                result.append("$cell")
            }
        }
        return result.toString()
    }
    fun checker(bs: Array<IntArray>, i: Int, ar: ArrayList<Int>): Boolean {
        val temp_ar = ArrayList<Int>()
        var check1 = true
        for (e in 0..8) {
            bs[i][e] = ar[e]
        }
        for (t in 0..8) {
            for (e in 0..i) {

                temp_ar.add(e, bs[e][t])
            }
            val temp_set = HashSet(temp_ar)
            if (temp_set.size < temp_ar.size) {
                check1 = false
                break
            } else {
                temp_ar.clear()
                temp_set.clear()
            }
        }
        return check1
    }

    fun checkPath(bs: Array<IntArray>, i: Int): Boolean {
        val temp_cP = ArrayList<Int>()
        var temp_setcP: MutableSet<Int> = HashSet()
        var denoter = true
        while (i == 3) {
            for (k in 0 until i) {
                for (e in 0..2) {
                    temp_cP.add(e, bs[k][e])
                }
            }
            temp_setcP = HashSet(temp_cP)
            if (temp_cP.size > temp_setcP.size) {
                denoter = false
                break

            } else {
                temp_cP.clear()
                temp_setcP.clear()

                for (k in 0 until i) {
                    for (e in 3..5) {
                        temp_cP.add(bs[k][e])
                    }
                }
                temp_setcP = HashSet(temp_cP)
                if (temp_cP.size > temp_setcP.size) {
                    denoter = false
                    break

                } else {
                    break
                }
            }
        }
        while (i == 6) {

            for (k in 3 until i) {
                for (e in 0..2) {
                    temp_cP.add(e, bs[k][e])
                }
            }
            temp_setcP = HashSet(temp_cP)
            if (temp_cP.size > temp_setcP.size) {
                denoter = false
                break

            } else {
                temp_cP.clear()
                temp_setcP.clear()

                for (k in 3 until i) {
                    for (e in 3..5) {
                        temp_cP.add(bs[k][e])
                    }
                }
                temp_setcP = HashSet(temp_cP)
                if (temp_cP.size > temp_setcP.size) {
                    denoter = false
                    break

                } else {
                    break
                }
            }

        }
        return denoter
    }

    fun sudoGen(): Array<IntArray> {
        val ar = ArrayList<Int>()
        val bs = Array(9) { IntArray(9) }
        for (i in 0..8) {
            ar.add(i + 1)
        }
        Collections.shuffle(ar)
        for (i in 0..0) {
            for (j in 0..8) {

                bs[i][j] = ar[j]
            }

        }
        var check = true
        var i = 0
        while (i < 9) {
            if (i == 3) {
                check = checkPath(bs, i)
                if (check == false)
                    i = i - 2

            }
            if (i == 6) {
                check = checkPath(bs, i)
                if (check == false) {
                    i = i - 2
                }
            }

            for (j in 0..8) {
                if (i > 0) {
                    check = checker(bs, i, ar)
                    if (check == false) {
                        i--
                        break
                    } else {
                        bs[i][j] = ar[j]
                    }
                }

            }
            Collections.shuffle(ar)
            i++
        }

        return bs
    }
}

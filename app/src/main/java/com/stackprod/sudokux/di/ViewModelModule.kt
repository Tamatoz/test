package com.stackprod.sudokux.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.stackprod.sudokux.model.view_model.*
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import com.stackprod.sudokux.MainScreen.MainScreenViewModel
import com.stackprod.sudokux.RandomGameScreen.RandomGameViewModel
import com.stackprod.sudokux.SettingsScreen.SettingsViewModel


@Suppress("unused")
@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(RandomGameViewModel::class)
    abstract fun bindRandomViewModel(randomViewModel: RandomGameViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SettingsViewModel::class)
    abstract fun bindSettingsViewModel(settingsViewModel: SettingsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MainScreenViewModel::class)
    abstract fun bindMainViewModel(mainViewModel: MainScreenViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

}
package com.stackprod.sudokux.NewGameScreen

import com.stackprod.sudokux.di.PerActivity
import dagger.Subcomponent


@Subcomponent(modules = arrayOf(NewGameModule::class))
@PerActivity
interface NewGameComponent {
    fun inject(activity: NewGameActivity)
}
package com.stackprod.sudokux.NewGameScreen

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import com.stackprod.sudokux.*
import com.stackprod.sudokux.LevelsScreen.LevelsActivity
import com.stackprod.sudokux.model.Level
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_new_game.*
import javax.inject.Inject

interface NewGameView: BaseView {

}

class NewGameActivity : BaseActivity<NewGameView, NewGamePresenter>(), NewGameView {

    override val mvpView: NewGameView
        get() = this

    @Inject
    override lateinit var presenter: NewGamePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_game)
        (application as App).appComponent.plus(NewGameModule()).inject(this)

        backButton.setOnClickListener { onBackPressed() }

        easyLevel.setOnClickListener {
            startLevel(0)
        }
        mediumLevel.setOnClickListener {
            startLevel(1)
        }
        hardLevel.setOnClickListener {
            startLevel(2)
        }
        maxLevel.setOnClickListener {
            startLevel(3)
        }

        val levels = Realm.getDefaultInstance().copyFromRealm(Realm.getDefaultInstance().where(Level::class.java).findAll())
        playScoreText.text = String.format("%d %%", (levels.count { it.solved && !it.isXMode }.toFloat() / levels.size * 100).toInt())
    }

    private fun startLevel(difLevel: Int){
        val intent = Intent(this, LevelsActivity::class.java)
        intent.putExtra("difLevel", difLevel)
        intent.putExtra("xMode", false)
        startActivity(intent)
    }
}
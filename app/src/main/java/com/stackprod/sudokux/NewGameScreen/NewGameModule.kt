package com.stackprod.sudokux.NewGameScreen

import com.stackprod.sudokux.di.PerActivity
import dagger.Module
import dagger.Provides
import dagger.Subcomponent


@Module
class NewGameModule {
    @Provides
    @PerActivity
    internal fun provideNewGamePresenter(): NewGamePresenter = NewGamePresenterImpl()

}
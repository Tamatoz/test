package com.stackprod.sudokux.NewGameScreen

import com.stackprod.sudokux.BasePresenter
import com.stackprod.sudokux.BasePresenterImpl

interface NewGamePresenter: BasePresenter<NewGameView> {

}

class NewGamePresenterImpl: BasePresenterImpl<NewGameView>(), NewGamePresenter {

}
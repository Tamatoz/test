package com.stackprod.sudokux.LevelsScreen

import android.os.Bundle
import com.stackprod.sudokux.model.Level
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_levels.*
import android.content.Intent
import android.view.View
import android.view.ViewGroup
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.stackprod.sudokux.*
import com.stackprod.sudokux.GameScreen.GameScreenActivity

import com.stackprod.sudokux.NewGameScreen.NewGameActivity
import com.stackprod.sudokux.ui.CardDialog
import io.realm.RealmResults
import javax.inject.Inject

interface LevelsView: BaseView {
    fun showRepeatDialog(levId: Int)
    fun openLevel(id: Int)
    fun showLevels(objs: LiveData<RealmResults<Level>>)
}

class LevelsActivity : BaseActivity<LevelsView, LevelsPresenter>(), LevelsView {

    override val mvpView: LevelsView
        get() = this

    @Inject lateinit var factory: LevelsViewModel.Factory

    private val viewModel by viewModels<LevelsViewModel> {
        factory.create(this)
    }

    @Inject
    override lateinit var presenter: LevelsPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (application as App).appComponent.plus(LevelsScreenModule()).inject(this)
        setContentView(R.layout.activity_levels)
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun onResume() {
        super.onResume()
        presenter.loadLevels(viewModel.difLevel, viewModel.isXMode)
    }

    override fun showLevels(objs: LiveData<RealmResults<Level>>) {
        objs.observe(this, Observer { data ->
            levels.adapter = LevelsAdapter(this, data, presenter)
        })

    }

    override fun showRepeatDialog(levId: Int){
        val dialog = CardDialog(this)
        dialog.setContent( getString(R.string.repeat_level),
                getString(android.R.string.cancel),
                getString(android.R.string.ok),
                View.OnClickListener {
                    dialog.dismiss()
                },
                View.OnClickListener {
                    val realm = Realm.getDefaultInstance()
                    val level = realm.where(Level::class.java).equalTo("id", levId).findFirst()
                    realm.executeTransaction {
                        level?.solved = false
                        level?.currentTime = 0
                    }
                    openLevel(levId)
                    dialog.dismiss()
                })
        dialog.show()
    }

    override fun openLevel(id: Int){
        var intent = Intent(this, GameScreenActivity::class.java)
        intent.putExtra("levelId", id)
        startActivity(intent)
    }

}
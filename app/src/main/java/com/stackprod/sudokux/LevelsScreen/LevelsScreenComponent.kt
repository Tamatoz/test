package com.stackprod.sudokux.LevelsScreen

import com.stackprod.sudokux.di.PerActivity
import dagger.Subcomponent

@Subcomponent(modules = arrayOf(LevelsScreenModule::class))
@PerActivity
interface LevelsScreenComponent {
    fun inject(activity: LevelsActivity)
}
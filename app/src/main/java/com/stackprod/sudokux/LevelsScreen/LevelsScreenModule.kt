package com.stackprod.sudokux.LevelsScreen

import com.stackprod.sudokux.di.PerActivity
import com.stackprod.sudokux.repository.GameRepository
import com.stackprod.sudokux.repository.GameRepositoryImpl
import dagger.Module
import dagger.Provides

@Module
class LevelsScreenModule {
    @Provides
    @PerActivity
    internal fun provideLevelScreenPresenter(gameRepository: GameRepository): LevelsPresenter = LevelsPresenterImpl(gameRepository)

}
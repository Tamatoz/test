package com.stackprod.sudokux.LevelsScreen

import android.content.Context

import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import android.widget.BaseAdapter
import android.widget.ImageView
import com.stackprod.sudokux.R
import com.stackprod.sudokux.model.Level
import com.stackprod.sudokux.ui.FontText


class LevelsAdapter(val context: Context, private val allElementDetails: List<Level>, private val presenter:LevelsPresenter) : BaseAdapter() {

    private val mInflater: LayoutInflater

    init {
        mInflater = LayoutInflater.from(context)
    }

    override fun getCount(): Int {
        return allElementDetails.size
    }

    override fun getItem(position: Int): Level {
        return allElementDetails[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        convertView = mInflater.inflate(R.layout.item_level, null)
        convertView.findViewById<FontText>(R.id.levelNum).text = (position + 1).toString()
        val starsCount = convertView.findViewById<ImageView>(R.id.starsCount)
        if(getItem(position).enable) {
            convertView.alpha = 1f
            convertView.setOnClickListener {
                if(getItem(position).solved)
                    presenter.showRepeatDialog(getItem(position).id)
                else presenter.openLevel(getItem(position).id)
            }

            if(getItem(position).solved){
                when(getItem(position).starsCount) {
                    0 -> starsCount.setImageResource(R.drawable.star0)
                    1 -> starsCount.setImageResource(R.drawable.star1)
                    2 -> starsCount.setImageResource(R.drawable.star2)
                    3 -> starsCount.setImageResource(R.drawable.star3)
                }
            } else {
                starsCount.setImageResource(R.drawable.star0)
            }
        } else {
            convertView.alpha = 0.3f
            convertView.setOnClickListener (null)
            starsCount.setImageResource(R.drawable.star0)
        }


        convertView.findViewById<FontText>(R.id.time).text = if(getItem(position).currentTime == 0) "--:--" else String.format("%d:%02d", getItem(position).currentTime / 60, getItem(position).currentTime % 60)
        return convertView
    }
}
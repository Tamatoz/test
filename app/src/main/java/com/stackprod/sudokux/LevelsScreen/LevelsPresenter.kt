package com.stackprod.sudokux.LevelsScreen

import com.stackprod.sudokux.BasePresenter
import com.stackprod.sudokux.BasePresenterImpl
import com.stackprod.sudokux.repository.GameRepository
import com.stackprod.sudokux.repository.GameRepositoryImpl

interface LevelsPresenter: BasePresenter<LevelsView> {
    fun showRepeatDialog(id: Int)
    fun openLevel(id: Int)
    fun loadLevels(difLevel: Int, isXMode: Boolean)
}

class LevelsPresenterImpl(val gameRepository: GameRepository): BasePresenterImpl<LevelsView>(), LevelsPresenter {
    override fun loadLevels(difLevel: Int, isXMode: Boolean) {
        view?.showLevels(gameRepository.getLevels(difLevel, isXMode))
    }

    override fun showRepeatDialog(id: Int){
        view?.showRepeatDialog(id)
    }

    override fun openLevel(id: Int){
        view?.openLevel(id)
    }
}
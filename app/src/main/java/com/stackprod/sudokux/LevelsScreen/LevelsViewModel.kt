package com.stackprod.sudokux.LevelsScreen

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.AbstractSavedStateVMFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import javax.inject.Inject

class LevelsViewModel constructor(
        savedStateHandle: SavedStateHandle) : ViewModel() {

    val difLevel : Int = savedStateHandle["difLevel"] ?:
    throw IllegalArgumentException("difLevel not passed")

    val isXMode : Boolean = savedStateHandle["xMode"] ?:
    throw IllegalArgumentException("xMode not passed")

    class Factory @Inject constructor() {
        fun create(owner: FragmentActivity): AbstractSavedStateVMFactory {
            return object : AbstractSavedStateVMFactory(owner, owner.intent.extras) {
                override fun <T : ViewModel> create(
                        key: String,
                        modelClass: Class<T>,
                        handle: SavedStateHandle
                ): T {
                    return LevelsViewModel(handle) as T
                }
            }
        }
    }
}